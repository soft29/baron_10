# -*- coding: utf-8 -*-
{
    'name': 'Weight product',
    'version': '1.0',
    'category': 'Sale',
    'summary': 'Weight product with task for stock',
    'description': '''
Add attribute can weight for product. 
After sale weight product system create task on stock.
    ''',
    'auto_install': False,
    'application':True,
        
    'author': 'IT Libertas',
    'website': 'http://itlibertas.net',
    'depends': [
        'product',
        'sale',
        'stock',
    ],
    'data': [ 
        'views/product_view.xml',
        'views/weighing_view.xml',
        'security/ir.model.access.csv',
            ],
    'qweb': [ 
    
            ],
    'js': [ 

            ],
    'demo': [ 

            ],
    'test': [ 

            ],
    'license': 'AGPL-3',
    'images': ['static/description/main.png'],
    'update_xml': [],
    'application':True,
    'installable': True,
    'private_category' : True,
}
