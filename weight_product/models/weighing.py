# -*- encoding: utf-8 -*-

from odoo import models, fields, api

class weighing_task(models.Model):
	_name = "weighing.task"
	_inherit = ['mail.thread']
	_description = "Weighing task"
	_order = 'state desc, create_date'

	state = fields.Selection([
		('waiting_weight', 'Waiting weight'),
		('done', 'Weighed'),
		('cancel', 'Canceled')],inverse='done_weight',default='waiting_weight')
	order_line = fields.Many2one('sale.order.line','Order Line', readonly=True)
	weight = fields.Float('Weight')
	product_id = fields.Many2one(related='order_line.product_id')
	product_uom_qty = fields.Float(related='order_line.product_uom_qty')
	order_id = fields.Many2one(related='order_line.order_id')
	user_id = fields.Many2one('res.users')

	@api.one
	def done_weight(self):
		if self.weight and self.order_line and self.state == 'waiting_weight':
			self.order_line.sudo().with_context(from_task_weight='True').write({'product_uom_qty' : self.weight})
			self.state = 'done'
			self.user_id = self.env.user

	def action_cancel(self):
		self.state = 'cancel'