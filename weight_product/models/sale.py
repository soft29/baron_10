# -*- encoding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class sale_order_line(models.Model):
	_inherit = 'sale.order.line'

	@api.one
	def sale_line_inv(self):
		if not self.env.context.get('from_task_weight'):
			if self.product_id.can_weight:
				vals = {
					'product_id':self.product_id,
					'product_uom_qty':self.product_uom_qty,
					'order_line':self.id,
					'order_id':self.order_id.id and self.order_id or False,
				}

				active_task = self.env['weighing.task'].search([('order_line','=',self.id),('state','=','waiting_weight')])
				if active_task:
					active_task.write(vals)
				else:
					self.env['weighing.task'].create(vals)

	@api.one
	def check_tasks(self):
		done_work = self.env['weighing.task'].search([('order_line','=',self.id),('state','=','done')])
		res_work = self.env['weighing.task'].search([('order_line','=',self.id),('state','=','waiting_weight')])
		if done_work and not res_work:
			self.weighing = True
		else:
			self.weighing = False



	weight_task_id = fields.One2many('weighing.task','order_line')
	weighing = fields.Boolean(string='Weighing',compute="check_tasks")

	product_id = fields.Many2one(inverse="sale_line_inv")
	product_uom_qty = fields.Float(inverse="sale_line_inv")

class sale_order(models.Model):
	_inherit = 'sale.order'

	@api.multi
	def action_button_confirm(self):
		for order in self:
			for order_line in order.order_line:
				if order_line.product_id.can_weight and not order_line.weighing:
					raise ValidationError(_("Not all task for weight completed."))
		return super(sale_order,self).action_button_confirm()
