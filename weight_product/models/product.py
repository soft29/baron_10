# -*- encoding: utf-8 -*-

from odoo import models, fields, api

class product_template(models.Model):
    _inherit = 'product.template'

    can_weight = fields.Boolean(string="Can weight")

