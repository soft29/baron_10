{
    'name': "SMS Support for Tele2",
    'version': "1.0",
    'author': "Soft29",
    'website': 'https://kupihleb.ru',
    'category': "Tools",
    'summary': "Allows 2 way sms conversations between leads/partners using the Tele2 gateway",
    'license':'LGPL-3',
    'data': [
        'data/sms.gateway.csv',      
        
        'security/ir.model.access.csv',

        'views/gateway_config.xml',
    ],
    'demo': [],
    'depends': [
        'base', 'crm', 'suvit_entity_sms'
    ],
    "external_dependencies": {
        'python': ['requests',
                   ]
    },
    'images':[
    ],
    'installable': True,
    'application': True,
}