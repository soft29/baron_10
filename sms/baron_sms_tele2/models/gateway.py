﻿# -*- coding: utf-8 -*-
import datetime
import hashlib

import requests
import logging
import traceback

from odoo import api, fields, models

_logger = logging.getLogger(__name__)


class Tele2Response:
    delivary_state = ""
    response_string = ""
    human_read_error = ""
    message_id = ""


def format_number(number):
    number = number.replace(" ", "")
    number = number.replace("+", "")
    return number
    
def getCorrectedNumber(phoneNum):
    isGoodNumber = True
    phone = ""
    for c in phoneNum:
        if c.isdigit():
            phone += c
    if len(phone) == 10 and phone.startswith("9"):
        phone = "7" + phone
    elif len(phone) == 11 and phone[1] == "9":
        if phone.startswith("8"):
            phone = "7" + phone[1:]
    elif len(phone) == 11 and phone[0] == "9" and phone[1] == "8":
        phone = "89" + phone[2:]
    else:
        isGoodNumber = False
        
    if isGoodNumber:
        return phone
    else:
        return None     


class Tele2Gateway(models.Model):
    _name = 'sms.tele2'
    _descripion = u'Шлюз SMSЦентр'

    _api = None

    ERRORS = [
        (1, u'Ошибка в параметрах.'),
        (2, u'Неверный логин или пароль.'),
        (3, u'Недостаточно средств на счете Клиента.'),
        (4, u'IP-адрес временно заблокирован из-за частых ошибок в запросах'),
        (5, u'Неверный формат даты.'),
        (6, u'Сообщение запрещено (по тексту или по имени отправителя).'),
        (7, u'Неверный формат номера телефона.'),
        (8, u'Сообщение на указанный номер не может быть доставлено.'),
        (9, u'Отправка более одного одинакового запроса на передачу SMS-сообщения'
            u' либо более пяти одинаковых запросов на получение стоимости сообщения в течение минуты.'),
    ]
    
    def send_message(self, sms_gateway_id,
                     from_number, to_number, sms_content,
                     my_model_name='', my_record_id=0, media=None, queued_sms_message=None, media_filename=False):
        
        sms_account = self.env['sms.account'].search([('id', '=', sms_gateway_id)])
        #gateway_id = self.env['sms.gateway'].search([('gateway_model_name', '=', 'sms.tele2')])

        from_number = format_number(from_number)
        #to_number = format_number(to_number)
        
        to_number_init = to_number
        to_number = getCorrectedNumber(to_number)
        #_logger.info("sms sent to_number (formatted): " + to_number)
        if to_number != None:
            try:
                resp = requests.get('http://newbsms.tele2.ru/api/',
                                     verify='false', 
                                     params={'operation': 'send',
                                           'login': sms_account.tele2_username,
                                           'password': sms_account.tele2_password,
                                           'msisdn': to_number,
                                           'shortcode': 'kupihleb.ru',
                                           'text': sms_content
                                           })
            except Exception as e:
                # TODO timeout errors
                _logger.error(traceback.format_exc())
                
            resp_content = resp.content
            if len(resp_content) > 18 and resp_content[8] == '-' and resp_content[13] == '-' and resp_content[18] == '-':
                status = 'successful'
                human_read_error = 'OK'   
                _logger.info('success')
            else:
                status = 'failed'
                human_read_error = resp_content         
                _logger.info('failed')
            _logger.info(resp_content)              
        else: 
            status = 'failed'
            to_number = to_number_init
            resp_content = "ERROR: wrong number format " + to_number
            human_read_error = resp_content

        sms_id = 0 #resp_dict.get('id', 0)
        cost = 0 #resp_dict.get('cost', 0)

        #my_model = self.env['ir.model'].search([('model','=', my_model_name)])
#         my_field = self.env['ir.model.fields'].search([('model_id', '=', my_model.id),
#                                                        ('name', '=', my_field_name)])

        # TODO what tz??
        sms_date = datetime.datetime.utcnow()
        Handler = self.env['suvit.sms.handler']
        Handler.run_all(sms={'direction': 'O',
                             'from_number': from_number,
                             'to_number': to_number,
                             'id': sms_id,
                             'body': sms_content,
                             'date': sms_date,
                             'cost': cost})

#         history_id = self.env['esms.history'].create({'account_id': sms_account.id,
#                                                       'gateway_id': gateway_id.id,
# 
#                                                       'status_code': status,
#                                                       'status_string': human_read_error,
#                                                       'delivary_error_string': human_read_error,
# 
#                                                       'direction': 'O',
#                                                       'from_mobile': from_number,
#                                                       'to_mobile': to_number,
#                                                       'sms_gateway_message_id': sms_id,
#                                                       'sms_content': sms_content,
#                                                       'my_date': sms_date,
# 
#                                                       'model_id': my_model.id,
#                                                       'record_id': my_record_id,
#                                                       'field_id': my_field.id,
# 
#                                                       'cost': cost})

        my_sms_response = Tele2Response()
        my_sms_response.delivary_state = status
        my_sms_response.response_string = resp_content
        my_sms_response.human_read_error = human_read_error
        my_sms_response.message_id = sms_id
        return my_sms_response

    def check_messages(self, account_id, message_id=""):
        pass



class Tele2Account(models.Model):
    _inherit = "sms.account"

    tele2_username = fields.Char(string='Tele2 API Username')
    tele2_password = fields.Char(string='Tele2 API Пароль')
    # smsc_sender = fields.Char(string=u'SMSЦентр API Отправитель') - use esms.verified.numbers

