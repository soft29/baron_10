﻿# -*- coding: utf-8 -*-
import pytz

from odoo import api, fields, models
from odoo.http import request
from dateutil.parser import parse
import logging

from openerp.addons.sms_frame.models.sms_template import mako_template_env

_logger = logging.getLogger(__name__)

def user_datetime(date_str):

    if request:
        env = request.env
        user_tz = pytz.timezone(env.context.get('tz') or env.user.tz or 'UTC')
    else:
        user_tz = pytz.utc

    # append 00-timezone so astimezone() wouldn't crash
    date_str += '+00:00'
    _logger.info(env.context.get('tz'))		
    _logger.info(env.user.tz)		
    _logger.info(user_tz)		
    _logger.info(parse(date_str).astimezone(user_tz))	
    # was:     return user_tz.localize(fields.Datetime.from_string(date_str)).astimezone(pytz.utc)
    return parse(date_str).astimezone(user_tz)


def ru_date_format(dt):
    return dt.strftime('%d.%m.%Y')

mako_template_env.filters.update(user_datetime=user_datetime,
                                 ru_date_format=ru_date_format)


class EsmsTemplate(models.Model):
    _inherit = "sms.template"

    # dont use translation on template
    template_body = fields.Text(translate=False)
