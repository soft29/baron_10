{
    'name': "Baron sms handlers",
    'version': "1.0",
    'author': "SUVIT LLC",
    'website': 'https://suvit.ru',
    'category': "Tools",
    'summary': "Handlers for inbound and outbound sms",
    'description': '''




* Handlers for inbound and outbound sms
* Track changes of sale.order state by send sms
    ''',
    'license':'LGPL-3',
    'data': [
        # 'security/ir.model.access.csv',
        'views/res_partner.xml',
        'views/sale_order.xml',
        'views/stock.xml',
    ],
    'demo': [],
    'depends': [
        'suvit_entity_sms',
        'sale' # for order
    ],
    'images':[
    ],
    'installable': True,
    'application': True,
}