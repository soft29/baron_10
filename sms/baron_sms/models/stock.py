﻿# -*- coding: utf-8 -*-
from odoo import api, fields, models


class Picking(models.Model):
    _name = 'stock.picking'
    _inherit = ['stock.picking',
                'suvit.sms.tracker']
    _sms_track_fields = ['state']

    #sms_track = fields.Boolean(related='sale_id.partner_id.sms_track')

    @api.multi
    def sms_action(self):
#         if len(self.ids) > 1:
#             mass_sms = self.env['esms.mass.sms'].create({'mass_sms_state': 'draft'})
#             for rec in self:
#                 if rec.sale_id.partner_id.mobile_e164:
#                     mass_sms.selected_records = [(4, rec.sale_id.partner_id.id)]
# 
#             return {
#                 'name': 'Массовая отправка SMS',
#                 'view_type': 'form',
#                 'view_mode': 'form',
#                 'res_model': 'esms.mass.sms',
#                 'res_id': mass_sms.id,
#                 'type': 'ir.actions.act_window',
#                 }
#         else:
        default_mobile = self.env['sms.number'].search([])[0]
        return {
            'name': 'Отправить SMS',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sms.compose',
            'target': 'new',
            'type': 'ir.actions.act_window',
            'context': {'default_from_mobile_id': default_mobile.id,'default_to_number':self.sale_id.partner_id.mobile, 
                        'default_record_id':self.id,'default_model':self._name}            
        }
