﻿# -*- coding: utf-8 -*-

from odoo import api, fields, models


class SmsHandler(models.Model):
    _inherit = 'suvit.sms.handler'

    @api.model
    def baron_add_sms_to_order_history(self, sms):
        # Custom logic
        from_phone = sms['from_number']

        valid_states = ['draft', 'sent']
        valid_states += ['sale', 'manual']

        last_valid_order = self.env['sale.order'].search([('partner_id.mobile', '=', from_phone),
                                                          ('state', 'in', valid_states)
                                                          ],
                                                         order='date_order desc',
                                                         limit=1)

        if last_valid_order:
            last_valid_order.message_post(body=sms['body'],
                                          subject="Получено SMS")
        else:
            # lead create
            self.env['crm.lead'].create({'name': u'Входящее SMS от %s' % from_phone,
                                         'phone': from_phone,
                                         'mobile': from_phone,
                                         'description': sms['body']})
