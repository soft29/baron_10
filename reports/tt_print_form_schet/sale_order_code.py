# -*- coding: utf-8 -*-

import time
# from openerp.report import report_sxw
# from openerp.osv import osv, fields
# from openerp.report.interface import report_int
from odoo.addons.jasper_reports import numeral

from odoo.report import report_sxw
from odoo import models, fields


class sale_order_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(sale_order_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update( {'time': time})

class account_invoice_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(account_invoice_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({'time': time})

class sale_order(models.Model):
    _name = 'sale.order'
    _inherit = 'sale.order'

    def _is_invoice(self):
        for row in self:
            row.is_invoice = False


    # Parse order name. Strip order prefix and leading zeroes
#     def _get_number_only(self):
#         res = {}
# 
#         for row in self.browse(cr, uid, ids, context):
#             seq_id = self.pool.get('ir.sequence').search(cr, uid, [('code', '=', 'sale.order')])
#             sequence = self.pool.get('ir.sequence').read(cr, uid, seq_id, ['padding', 'active'])[0]
#             if sequence and sequence.get('active'):
#                 padding = sequence.get('padding')
#                 padding = 0 - int(padding)
#                 res[row.id] = row.name[padding:].lstrip('0')
# 
#         return res

    # Parse order name. Strip order prefix and leading zeroes
    def _get_number_only(self):
        for row in self:
            seq_id = self.env['ir.sequence'].search([('code', '=', 'sale.order')])
            #rec = self.env['ir.sequence'].browse(seq_id)
            sequence = seq_id.read(['padding', 'active'])[0]
            if sequence and sequence.get('active'):
                padding = sequence.get('padding')
                padding = 0 - int(padding)
                row.number_only = row.name[padding:].lstrip('0')
 

    def _get_price_in_words(self):
 
        for row in self:
            rubles = numeral.rubles(int(row.amount_total))
            copek_tmp = round(row.amount_total - int(row.amount_total))
            copek = numeral.choose_plural(int(copek_tmp), (u"копейка", u"копейки", u"копеек"))
            row.price_in_words = ("%s %02d %s")%(rubles, copek_tmp, copek)            


    def _get_orders_count(self):
        for row in self:
            row.orders_count = len(row.order_line)

    
    

#     _columns = {
#         'is_invoice': fields.function(_is_invoice, type='boolean'),
#         'number_only': fields.function(_get_number_only, type='char'),
#         'price_in_words':fields.function(_get_price_in_words, type='char'),
#         'orders_count': fields.function(_get_orders_count, type='integer'),
#     }
#     
    is_invoice = fields.Boolean(compute="_is_invoice")
    number_only = fields.Char(compute="_get_number_only")
    price_in_words = fields.Char(compute="_get_price_in_words")
    orders_count = fields.Integer(compute="_get_orders_count")
            
sale_order()


class account_invoice(models.Model):
    _name = 'account.invoice'
    _inherit = 'account.invoice'    
    
    def _get_number_only(self):
        for row in self:
            seq_id = self.env['ir.sequence'].search([('code', '=', 'sale.order')])
            #rec = self.env['ir.sequence'].browse(seq_id)
            sequence = seq_id.read(['padding', 'active'])[0]
            if sequence and sequence.get('active'):
                padding = sequence.get('padding')
                padding = 0 - int(padding)
                row.number_only = row.name[padding:].lstrip('0')

    def _is_invoice(self):
        for row in self:
            row.is_invoice = True

    def _get_price_in_words(self):
        for row in self:
            rubles = numeral.rubles(int(row.amount_total))
            copek_tmp = round(row.amount_total - int(row.amount_total))
            copek = numeral.choose_plural(int(copek_tmp), (u"копейка", u"копейки", u"копеек"))
            row.price_in_words = ("%s %02d %s")%(rubles, copek_tmp, copek)      

            
    def _get_invoices_count(self):
        for row in self:
            row.invoices_count = len(row.invoice_line_ids)            

# commented soft29
#     def invoice_print(self, cr, uid, ids, context=None):
#         '''
#         This function prints the invoice and mark it as sent, so that we can see more easily the next step of the workflow
#         '''
#         assert len(ids) == 1, 'This option should only be used for a single id at a time.'
#         self.write(cr, uid, ids, {'sent': True}, context=context)
#         datas = {
#              'ids': ids,
#              'model': 'account.invoice',
#              'form': self.read(cr, uid, ids[0], context=context)
#         }
#         return {
#             'type': 'ir.actions.report.xml',
#             'report_name': 'account.invoice.schet',
#             'datas': datas,
#             'nodestroy' : True
#         }


#     _columns = {
#         'is_invoice': fields.function(_is_invoice, type='boolean'),
#         'number_only': fields.function(_get_number_only, type='char'),
#         'price_in_words':fields.function(_get_price_in_words, type='char'),
#         'invoices_count': fields.function(_get_invoices_count, type='integer'),
#     }
    is_invoice = fields.Boolean(compute="_is_invoice")
    number_only = fields.Char(compute="_get_number_only")
    price_in_words = fields.Char(compute="_get_price_in_words")
    invoices_count = fields.Integer(compute="_get_invoices_count")    
    
account_invoice()
