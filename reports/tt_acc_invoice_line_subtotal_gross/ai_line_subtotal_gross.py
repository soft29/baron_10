# -*- coding: utf-8 -*-
from odoo import models, fields
from odoo import api
import odoo.addons.decimal_precision as dp


class account_invoice_line(models.Model):
    _name = 'account.invoice.line'
    _inherit = 'account.invoice.line'

    def _amount_line(self):
        tax_obj = self.env['account.tax']
        cur_obj = self.env['res.currency']
        for line in self:
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            
            #soft29
            if len(self) == 0:
                company_id = self.env.user.company_id
            else:
                company_id = self[0].company_id
            currency = company_id.currency_id            
            t = tax_obj.browse(line.invoice_line_tax_id.id)
            
            taxes = t.compute_all(price, currency, line.quantity, line.product_id, line.invoice_id.partner_id)
            line.price_subtotal = taxes['total_included']
            if line.invoice_id:
                cur = line.invoice_id.currency_id
                c = cur_obj.browse(cur.id)
                
                line.price_subtotal = c.round(taxes['total_included'])


#     _columns = {
#         'price_subtotal': fields.function(_amount_line, string='Amount', type="float",
#                                           digits_compute=dp.get_precision('Account'), store=True),
#     }
    
    price_subtotal = fields.Float(compute="_amount_line", string='Amount', 
                            digits=dp.get_precision('Account'),  store=True)    

account_invoice_line()
# 
# soft29
#
# class account_invoice(models.Model):
#     _inherit = 'account.invoice'
# 
#     @api.multi
#     def _get_analytic_lines(self):
#         res = super(account_invoice, self)._get_analytic_lines()
#         try:
#             precision = dp.get_precision('Product Price')(self._cr)[1]
#         except:
#             raise Exception(_("Error"),
#                                  _("I can't get decimal precision of Product Price. Ask for help from developers."))
#         for invoice_line in res:
#             tax_total = 0.0
#             for tax in invoice_line['taxes']:
#                 if tax.price_include and tax.include_base_amount:
#                     tax_total += tax.amount
#             invoice_line['price'] = round(float(invoice_line['price'])/(1+tax_total), precision)
#         return res
# 
#     def _amount_all(self, cr, uid, ids, name, args, context=None):
#         res = {}
#         for invoice in self.browse(cr, uid, ids, context=context):
#             res[invoice.id] = {
#                 'amount_untaxed': 0.0,
#                 'amount_tax': 0.0,
#                 'amount_total': 0.0
#             }
#             for line in invoice.invoice_line:
#                 res[invoice.id]['amount_untaxed'] += line.price_subtotal
#             for line in invoice.tax_line:
#                 res[invoice.id]['amount_tax'] += line.amount
#             res[invoice.id]['amount_untaxed'] -= res[invoice.id]['amount_tax']
#             res[invoice.id]['amount_total'] = res[invoice.id]['amount_tax'] + res[invoice.id]['amount_untaxed']
#         return res
# 
#     def _get_invoice_tax(self, cr, uid, ids, context=None):
#         result = {}
#         for tax in self.pool.get('account.invoice.tax').browse(cr, uid, ids, context=context):
#             result[tax.invoice_id.id] = True
#         return result.keys()
# 
#     def _get_invoice_line(self, cr, uid, ids, context=None):
#         result = {}
#         for line in self.pool.get('account.invoice.line').browse(cr, uid, ids, context=context):
#             result[line.invoice_id.id] = True
#         return result.keys()
# 
#     _columns = {
#          'amount_untaxed': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Subtotal',
#                                            track_visibility='always',
#             store={
#                 'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['invoice_line'], 20),
#                 'account.invoice.tax': (_get_invoice_tax, None, 20),
#                 'account.invoice.line': (_get_invoice_line, ['price_unit', 'invoice_line_tax_id', 'quantity',
#                                                              'discount', 'invoice_id'], 20),
#             },
#             multi='all'),
#         'amount_tax': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Tax',
#             store={
#                 'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['invoice_line'], 20),
#                 'account.invoice.tax': (_get_invoice_tax, None, 20),
#                 'account.invoice.line': (_get_invoice_line, ['price_unit', 'invoice_line_tax_id', 'quantity',
#                                                              'discount', 'invoice_id'], 20),
#             },
#             multi='all'),
#         'amount_total': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Total',
#             store={
#                 'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['invoice_line'], 20),
#                 'account.invoice.tax': (_get_invoice_tax, None, 20),
#                 'account.invoice.line': (_get_invoice_line, ['price_unit', 'invoice_line_tax_id', 'quantity',
#                                                              'discount', 'invoice_id'], 20),
#             },
#             multi='all'),
#     }
# account_invoice()