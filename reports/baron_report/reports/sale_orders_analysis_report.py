# -*- coding: utf-8 -*-

from openerp.report import report_sxw
from openerp.osv.orm import except_orm
from openerp import _
import logging

_logger = logging.getLogger(__name__)


class sale_orders_analysis(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(sale_orders_analysis, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({})

report_sxw.report_sxw(
    'report.sale.orders.analysis',
    'sale.order',
    'baron_report/reports/sale_orders_analysis_report.rml',
    parser=sale_orders_analysis,
    header=False,
)
