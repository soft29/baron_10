# -*- coding: utf-8 -*-

from openerp import models, fields, api, tools
from tempfile import NamedTemporaryFile
from collections import namedtuple
from datetime import datetime, time
from dateutil import tz
from operator import attrgetter
import base64
import xlsxwriter
import logging

_logger = logging.getLogger(__name__)


class sale_order(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def get_address(self, replace_returns=False):
        self.ensure_one()
        address = self.partner_shipping_id.contact_address or ''
        phone = self.partner_shipping_id.phone or self.partner_shipping_id.parent_id.phone or self.partner_shipping_id.company_id.phone or ''
        res = address + '\n' + phone
        if replace_returns:
            res = res.replace('\n', ' ')
        return res


class sale_order_analysis_xls(models.TransientModel):
    _name = 'sale.order.analysis.xls'

    xls_file = fields.Binary(u'Файл результатов')
    name_file = field_name = fields.Char(string='Filename')

    def _get_report_fields(self):
        # (column_title, value_getter)
        def get_date(order):
            do = fields.Datetime.from_string(order.date_order)
            return fields.Datetime.context_timestamp(order, do).strftime('%d.%m.%Y')

        report_fields = (
            (u"№ заказа", lambda l: l.name),
            (u"Адрес", lambda l: l.get_address(replace_returns=True)),
            (u"Сумма", lambda l: l.amount_total),
            (u"Комментарий", lambda l: l.note),
            (u'Период доставки', lambda l: l.delivery_period_id.name),
            (u"Дата", get_date),
        )
        return report_fields

    @api.multi
    def generate_report(self, ids=False, title='', file_name=''):
        self.ensure_one()

        out_file = NamedTemporaryFile(suffix=".xlsx", delete=False)
        out_file.close()
        self.name_file = file_name + '.xlsx'

        workbook = xlsxwriter.Workbook(out_file.name)

        worksheet = workbook.add_worksheet()
        table_header_items = self._get_report_fields()
        format_main_header = workbook.add_format({
            'bold': True,
            'font_size': 11,
            'fg_color': '#f2f2f2',
            'align': 'vcenter',
            'border': True,
        })

        # Первые 3 строки под заголовок отчёта

        col_letter = chr(len(table_header_items) + 96).upper()
        worksheet.merge_range(
            'A1:{c}3'.format(c=col_letter),
            title,
            format_main_header,
        )

        # Заголовок таблицы - 4 и 5 строки

        for counter, header in enumerate(table_header_items):
            col_name = header[0]
            col_letter = chr(counter + 97).upper()
            worksheet.merge_range(
                '{c}4:{c}5'.format(c=col_letter),
                col_name,
                format_main_header,
            )
            worksheet.set_column('{c}:{c}'.format(c=col_letter), 25)

        # Тело таблицы

        sale_order_ids = ids
        for row_counter, sale_order in enumerate(sale_order_ids, 5):
            for col_counter, header in enumerate(table_header_items):
                getter = header[1]
                value = getter(sale_order) or ''
                worksheet.write(
                    row_counter,
                    col_counter,
                    value,
                )
        workbook.close()

        with open(out_file.name, 'rb') as r:
            self.xls_file = base64.b64encode(r.read())

        return {
            'name': u'Выгрузка заказов продаж',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': self.env.ref('baron_report.sale_order_export_xls_view').id,
            'res_model': 'sale.order.analysis.xls',
            'context': "{}",
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'res_id': self.id,
        }
