#coding: utf-8
from odoo import models, fields, api, _

class sale_order_line(models.Model):
    _inherit = "sale.order.line"

    correct_name = fields.Char(compute="_get_name_from_modification",string="Correct name")
    correct_quantity = fields.Float(compute="_get_qty_from_modification",string="Correct qty")
    correct_price_unit = fields.Float(compute="_get_price_unit_from_modification",string="Correct price_unit")

    @api.one
    def _get_name_from_modification(self):
        res = ''
        if self.product_id:
            res = self.product_id.product_tmpl_id.name
            if self.product_id.attribute_value_ids:
                res += ' ('
                for attr in self.with_context(active_id=self.product_id.product_tmpl_id.id).product_id.attribute_value_ids:
                    if not attr.pack_true:
                        res += ' ' + attr.name
                        res += ')'
                        self.correct_name = res
            self.correct_name = res                        

    @api.one
    def _get_qty_from_modification(self):
        coef = 1
        if self.product_id:
            for attr in self.with_context(active_id=self.product_id.product_tmpl_id.id).product_id.attribute_value_ids:
                if attr.pack_true:
                    coef *= attr.pack_qty
        self.correct_quantity = self.product_uom_qty * coef

    @api.one
    def _get_price_unit_from_modification(self):
        self.correct_price_unit = self.price_subtotal / self.correct_quantity


class OrderReport(models.Model):
    _name = 'sale.order.report'
    _inherit = 'sale.order'
    _table = 'sale_order'
    _needaction = False
