# -*- coding: utf-8 -*-
from openerp import api, fields, models, _

import random


class product_product(models.Model):
    _inherit = "product.product"

    @api.multi
    def get_attributes_values_for_label(self):
        for product in self:
            #variant = ", ".join([v.name for v in product.attribute_value_ids])
            variant = ''
            for v in product.attribute_value_ids:
                variant += v.name
                for price_id in v.price_ids:
                    if price_id.product_tmpl_id.id == product.product_tmpl_id.id:
                        if price_id.pack_true:
                            variant += ' x ' + ('%.2f' % price_id.pack_qty) + u" Шт."
                variant += ', '
            product.attributes_values_for_label = variant and "%s" % (
                variant[:-2]) or ''

    attributes_values_for_label = fields.Char(
        string='Attributes values',
        compute=get_attributes_values_for_label)
