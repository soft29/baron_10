#coding: utf-8
from odoo import models, fields, api, _
from odoo.addons.jasper_reports import numeral

class account_invoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def _weight_nett_in_words(self):
        for invoice in self:
            weight = 0

            for line in invoice.invoice_line_ids:
                if line.product_id.weight:
                    weight += line.product_id.weight*line.correct_quantity

            if weight:
                self.weight_nett_in_words = numeral.in_words(weight)   
            else:
                self.weight_nett_in_words = ""

    @api.multi
    def _weight_brutt_in_words(self):
        for invoice in self:
            weight = 0
            for line in invoice.invoice_line_ids:
                if line.product_id.weight:
                    weight += line.product_id.weight*line.correct_quantity
            if weight:
                self.weight_brutt_in_words = numeral.in_words(weight)
            else:
                self.weight_brutt_in_words = ""

    @api.multi
    def _place_in_words(self):
        for invoice in self:
            place = 0
            for line in invoice.invoice_line_ids:
                if line.quantity:
                    place += line.quantity
            if place:
                self.place_in_words = numeral.in_words(place)
            else:
                self.place_in_words = ""

    weight_nett_in_words = fields.Char(compute="_weight_nett_in_words")
    weight_brutt_in_words = fields.Char(compute="_weight_brutt_in_words")
    place_in_words = fields.Char(compute="_place_in_words")

class account_invoice_line(models.Model):
    _inherit = "account.invoice.line"

    @api.one
    def _get_name_from_modification(self):
        res = ''
        if self.product_id:
            res = self.product_id.product_tmpl_id.name
            if self.product_id.attribute_value_ids:
                res += ' ('
                for attr in self.with_context(active_id=self.product_id.product_tmpl_id.id).product_id.attribute_value_ids:
                    if not attr.pack_true:
                        res += ' ' + attr.name
                res += ')'
        self.correct_name = res

    @api.one
    def _get_qty_from_modification(self):
        coef = 1
        if self.product_id:
            for attr in self.with_context(active_id=self.product_id.product_tmpl_id.id).product_id.attribute_value_ids:
                if attr.pack_true:
                    coef *= attr.pack_qty
        self.correct_quantity = self.quantity * coef
        self.correct_coef = coef

    @api.one
    def _get_price_unit_from_modification(self):
        self.correct_price_unit = self.price_subtotal / self.correct_quantity

    correct_name = fields.Char(compute="_get_name_from_modification",string="Correct name")
    correct_quantity = fields.Float(compute="_get_qty_from_modification",string="Correct qty")
    correct_coef = fields.Float(compute="_get_qty_from_modification",string="Correct coef")
    correct_price_unit = fields.Float(compute="_get_price_unit_from_modification",string="Correct price_unit")
