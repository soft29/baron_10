import odoo
# import openerp.modules.registry
# from openerp.addons.base.ir.ir_qweb import AssetsBundle #, QWebTemplateNotFound
# from openerp.modules import get_module_resource
# from openerp.tools import topological_sort
# from openerp.tools.translate import _
from odoo import http
from odoo.http import Controller, route, request
from odoo.tools import html_escape
from odoo.http import content_disposition, dispatch_rpc, request, serialize_exception as _serialize_exception
from odoo.tools.safe_eval import safe_eval
# 
# from openerp.http import request, serialize_exception as _serialize_exception
# from openerp.addons.web.controllers.main import Reports,serialize_exception,content_disposition\
from odoo.addons.web.controllers.main import Reports,serialize_exception
# from openerp.osv import osv
import simplejson
import time
import logging
_logger = logging.getLogger(__name__)

class ReportsITL(Reports):
    
    @http.route('/web/tmp', type='http', auth="user")
    def tmp(self):
        rec = request.env['sale.order'].browse(11)
        pass

    @http.route('/web/report', type='http', auth="user")
    @serialize_exception
    def index(self, action, token):
        response = Reports().index(action, token)
        action = simplejson.loads(action)
        context = dict(request.context)
        context.update(action["context"])
        type = action.get('report_type')
        if type == 'pdf':
            reportname = action.get('report_name')
            _logger.info(u'Getting attachment name for {}'.format(reportname))

            docids = action.get('context').get('active_ids')
            if docids:
                report_obj = request.env['report'] #request.registry['report']
                #reports = request.env['ir.actions.report.xml']
                #cr, uid = request.cr, request.uid

                #res_id = reports.with_context(context).search([('report_name', '=', action['report_name']),])
                report = report_obj.env['ir.actions.report.xml'].with_context(context).search([('report_name', '=', action['report_name'])], limit=1)
                if report:
                    #report = reports.browse(res_id[0])

                    ids = docids
                    _logger.info(u'Found document id: {}'.format(ids[0]))
                    attachment = report_obj._check_attachment_use(ids, report)
                    try:
                        filename = attachment[ids[0]]
                        _logger.info(u'Found report filename: {}'.format(filename))
                        response.headers.set('Content-Disposition', content_disposition(filename))
                    except KeyError:
                        if ids[0] in attachment['loaded_documents']:
                            try:
                                obj = report_obj.pool[report.model].browse(ids[0])
                                filename = eval(report.attachment, {'object': obj, 'time': time})
                                _logger.info('Found report filename in attachment: {}'.format(filename))
                                response.headers.set('Content-Disposition', 'attachment; filename=%s;' % filename)
                            except KeyError:
                                _logger.info('Report is loaded from attachment')
                        else:
                            _logger.warning('No report filename found, using default')
        return response