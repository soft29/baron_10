# coding: utf-8
import logging
from datetime import datetime, timedelta

import pytz

from odoo import models, fields, api, _

_logger = logging.getLogger(__name__)


class sale_orders_stats(models.TransientModel):
    _name = 'sale.orders.stats'
    _description = 'Get sale orders stats wizard'

    date_from = fields.Date(u"Дата заказа от", required=True)
    date_to = fields.Date(u"Дата заказа до", required=True)
    status = fields.Selection([
            ("draft", u"Черновик"),
            ("in_work", u"В работе"),
            ("done", u"Выполнен"),
        ],
        u"Статус заказа",
        required=True,
    )
    sale_orders_ids = fields.One2many(string="Заказы продаж",
                                      comodel_name="sale.order",
                                      compute='_compute_sale_orders_ids')

    @api.multi
    def print_report_pdf(self):
        report_name = 'report.sale.orders.analysis'
        res = self.env['report'].get_action(self, report_name)
        return res

    @api.multi
    def print_report_xls(self):
        status_map = {
            "draft": u"Черновик",
            "in_work": u"В работе",
            "done": u"Выполнен",
        }

        format_date = lambda d: datetime.strptime(d, '%Y-%m-%d').strftime('%d.%m.%Y')
        status = status_map[self.status]

        title = u'Отчёт по заказам продаж\nВ период от {} до {}\nТип заказов: {}'.format(
            format_date(self.date_from),
            format_date(self.date_to),
            status,
        )
        file_name = u'Заказы_{}_({}_{})'.format(
            status.replace(' ','_'),
            self.date_from,
            self.date_to,
        )
        xls_report = self.env['sale.order.analysis.xls'].create({})
        return xls_report.generate_report(self.sale_orders_ids, title=title, file_name=file_name)

    @api.one
    @api.depends('date_from', 'date_to', 'status')
    def _compute_sale_orders_ids(self):
        if self.status == 'draft':
            states = ['draft', 'sent']
        elif self.status == 'in_work':
            states = ['waiting_date', 'progress', 'manual']
        elif self.status == 'done':
            states = ['done']
        else:
            states = []

        user_tz = pytz.timezone(self.env.context.get('tz') or self.env.user.tz or 'UTC')
        df = user_tz.localize(fields.Datetime.from_string(self.date_from)).astimezone(pytz.utc)
        dt = user_tz.localize(fields.Datetime.from_string(self.date_to)).astimezone(pytz.utc) + timedelta(days=1)

        self.sale_orders_ids = self.env['sale.order'].search([
            ('date_order', '>=', fields.Datetime.to_string(df)),
            ('date_order', '<=', fields.Datetime.to_string(dt)),
            ('state', 'in', states),
        ])
