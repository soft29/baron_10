# coding: utf-8
import logging
import base64
from functools import partial
from datetime import datetime, timedelta
from tempfile import NamedTemporaryFile

import pytz
import xlsxwriter

from openerp import models, fields, api, _

_logger = logging.getLogger(__name__)


class OrderLineStats(models.TransientModel):
    _name = 'sale.order.line.stats'
    _description = u'Мастер Отчета по номенклатуре'

    date_from = fields.Date(u"Дата заказа от", required=True)
    date_to = fields.Date(u"Дата заказа до", required=True)
    status = fields.Selection([
            ("draft", u"Корзины"),
            ("in_work", u"В работе"),
            ("done", u"Выполнен"),
        ],
        u"Статус заказа",
    )
    sale_order_ids = fields.One2many(string="Заказы продаж",
                                     comodel_name="sale.order",
                                     compute='_compute_sale_orders_ids')

    xls_file = fields.Binary(u'Файл результатов')
    name_file = field_name = fields.Char(string='Filename')

    @api.one
    @api.constrains('date_from', 'date_to')
    def wrong_dates(self):
        if self.date_from > self.date_to:
            raise api.Warning('Дата начала %s должна быть меньше или равна даты конца %s' % (self.date_from,
                                                                                             self.date_to))

    @api.one
    @api.depends('date_from', 'date_to', 'status')
    def _compute_sale_orders_ids(self):
        user_tz = pytz.timezone(self.env.context.get('tz') or self.env.user.tz or 'UTC')
        df = user_tz.localize(fields.Datetime.from_string(self.date_from)).astimezone(pytz.utc)
        dt = user_tz.localize(fields.Datetime.from_string(self.date_to)).astimezone(pytz.utc) + timedelta(days=1)

        domain = [
            ('date_order', '>=', fields.Datetime.to_string(df)),
            ('date_order', '<=', fields.Datetime.to_string(dt)),
        ]

        states = []
        if self.status == 'draft':
            states = ['sent',]
        if self.status == 'in_work':
            states = ['waiting_date', 'progress', 'manual',]
        if self.status == 'done':
            states = ['done',]
        if states:
            domain.append(('state', 'in', states))

        self.sale_order_ids = self.env['sale.order'].search(domain)

    @api.multi
    def print_report_xls(self):
        status_map = {
            "draft": u"Черновик",
            "in_work": u"В работе",
            "done": u"Выполнен",
        }

        format_date = lambda d: datetime.strptime(d, '%Y-%m-%d').strftime('%d.%m.%Y')
        status = status_map.get(self.status, u'Все')

        title = u'Отчёт по номенклатуре\nВ период от {} до {}\nТип заказов: {}'.format(
            format_date(self.date_from),
            format_date(self.date_to),
            status,
        )
        file_name = u'Номенклатура_продаж_{}_({}_{})'.format(
            status.replace(' ','_'),
            self.date_from,
            self.date_to,
        )
        return self.generate_report(self.sale_order_ids
                                        .mapped('order_line.product_id'),
                                    title=title, file_name=file_name)

    def _get_report_fields(self):
        # (column_title, value_getter)
        cols = [
            (u"Номенклатура", lambda p: u'%s (%s)' %(p.name, p.attributes_values_for_label), {}),
        ]

        def generate_dates(date_from, date_to):
            while date_from <= date_to:
                yield date_from
                date_from += timedelta(days=1)

        def check_date(date, order):
            do = fields.Datetime.from_string(order.date_order)
            return fields.Datetime.context_timestamp(order, do).date() == date

        # cache
        date2ol = {}
        def get_count(date, product, orders=self.sale_order_ids):
            # from sale.order.line
            # 'product_uom_qty': fields.float('Quantity', digits_compute= dp.get_precision('Product UoS'), required=True, readonly=True, states={'draft': [('readonly', False)]}),
            # 'product_uom': fields.many2one('product.uom', 'Unit of Measure ', required=True, readonly=True, states={'draft': [('readonly', False)]}),
            # 'product_uos_qty': fields.float('Quantity (UoS)' ,digits_compute= dp.get_precision('Product UoS'), readonly=True, states={'draft': [('readonly', False)]}),
            # 'product_uos': fields.many2one('product.uom', 'Product UoS'),
            # print 'OLReport.get_count', date, product
            # import pdb; pdb.set_trace()
            if date in date2ol:
                ol = date2ol[date]
            else:
                ol = date2ol[date] = (orders.filtered(partial(check_date, date))
                                            .mapped('order_line'))
            return sum(ol.filtered(lambda ol: ol.product_id == product)
                         .mapped('product_uom_qty') or [0])

        for date in generate_dates(fields.Date.from_string(self.date_from),
                                   fields.Date.from_string(self.date_to)):
            cols.append((date.strftime('%d.%m.%Y'),
                         partial(get_count, date),
                         {'width': 9},
                        ))

        return cols

    @api.multi
    def generate_report(self, product_ids, title='', file_name=''):
        self.ensure_one()

        out_file = NamedTemporaryFile(suffix=".xlsx", delete=False)
        out_file.close()
        self.name_file = file_name + '.xlsx'

        workbook = xlsxwriter.Workbook(out_file.name)
        worksheet = workbook.add_worksheet()
        cols = self._get_report_fields()
        format_main_header = workbook.add_format({
            'bold': True,
            'font_size': 11,
            'fg_color': '#f2f2f2',
            'align': 'vcenter',
            'border': True,
        })

        # Первые 3 строки под заголовок отчёта
        worksheet.merge_range(
            first_row=0, first_col=0,
            last_row=2, last_col=len(cols) - 1,
            data=title,
            cell_format=format_main_header,
        )

        # Заголовок таблицы - 4 и 5 строки
        for counter, header in enumerate(cols):
            col_name = header[0]
            worksheet.merge_range(
                first_row=3, first_col=counter,
                last_row=4, last_col=counter,
                data=col_name,
                cell_format=format_main_header,
            )
            worksheet.set_column(firstcol=counter, lastcol=counter,
                                 width=header[2].get('width', 25))

        # Тело таблицы
        # print 'OLReport.products', product_ids
        for row_counter, product_id in enumerate(product_ids, 5):
            for col_counter, header in enumerate(cols):
                getter = header[1]
                value = getter(product_id)
                # print 'OLReport', product_id, header[0], value
                # import pdb; pdb.set_trace()
                worksheet.write(
                    row_counter,
                    col_counter,
                    value,
                )
        workbook.close()

        with open(out_file.name, 'rb') as r:
            self.xls_file = base64.b64encode(r.read())

        return {
            'name': u'Выгрузка отчета о номенклатуре',
            'view_mode': 'form',
            'view_id': self.env.ref('baron_report.sale_order_line_export_xls_view').id,
            'res_model': self._name,
            'res_id': self.id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'context': "{}",
        }
