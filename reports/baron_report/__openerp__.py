# -*- coding: utf-8 -*-
{
    'name': 'Baron report',
    'version': '1.0',
    'category': 'Category',
    'summary': 'Summary',
    'description': '''
Modification standart report for TD Baron
    ''',
    'auto_install': False,
    'application':True,
    'author': 'IT Libertas, SUVIT LLC',
    'website': 'http://itlibertas.net, https://suvit.ru',
    'depends': [
        'baron_package_attr',
        'baron_shelf',
        'tt_print_form_schet',
        'tt_print_form_schet_factura',
        'tt_print_form_torg12',
        'sale',
        'account',
        'web',
       # 'sale_layout',
    ],
    'data': [
        'security/ir.model.access.csv',

        'data/data_report.xml',

        'views/layouts.xml',

        'wizard/sale_orders_stats.xml',
        'wizard/sale_order_line_stats.xml',

        'reports/sale_order.xml',
        # 'reports/report_invoice.xml',
        'reports/sale_order_analysis_xls.xml',
        'reports/report_saleorder.xml',
    ],
    'qweb': [
    ],
    'js': [
    ],
    'demo': [
    ],
    'test': [
    ],
    'license': 'AGPL-3',
    'images': ['static/description/main.png'],
    'update_xml': [],
    'application':True,
    'installable': True,
    'private_category': True,
    'external_dependencies': {
        'python': ['xlsxwriter'],
    },
}
