# -*- coding: utf-8 -*-

import time
from odoo import models, fields
from odoo import api
from odoo.report import report_sxw



class invoice_form(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(invoice_form, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({'time': time})

class account_invoice(models.Model):
    def _get_number_only(self):
        for row in self:
            if not row.number:
                raise Exception(_('Error!'), _('You must confirm invoice!'))            
            seq_id = self.env['ir.sequence'].search([('code', '=', 'sale.order')], limit=1)
            #rec = self.env['ir.sequence'].browse(seq_id)
            sequence = seq_id.read(['padding', 'active'])[0]
            if sequence and sequence.get('active'):
                padding = sequence.get('padding')
                padding = 0 - int(padding)
                row.number_only = row.number[padding:].lstrip('0')

    def _get_pos_in_words(self):
        for row in self:
            row.pos_in_words = numeral.in_words(len(row.invoice_line_ids))

    def _get_price_in_words(self):
        for row in self:
            rubles = numeral.rubles(int(row.amount_total))
            copek_tmp = round(row.amount_total - int(row.amount_total))
            copek = numeral.choose_plural(int(copek_tmp), (u"копейка", u"копейки", u"копеек"))
            row.price_in_words = ("%s %02d %s")%(rubles, copek_tmp, copek)   

    def _get_invoices_count(self):
        for row in self:
            row.invoices_count = len(row.invoice_line_ids)    

    def get_partner_info(self):
        return super(account_invoice, self).get_partner_info()

    _name = 'account.invoice'
    _inherit = 'account.invoice'
#     _columns = {
#         'number_only': fields.function(_get_number_only, type='char'),
#         'price_in_words': fields.function(_get_price_in_words, type='char'),
#         'pos_in_words': fields.function(_get_pos_in_words, type='char'),
#         'invoices_count': fields.function(_get_invoices_count, type='integer'),
#     }
    
    number_only = fields.Char(compute="_get_number_only")
    number_only = fields.Char(compute="_get_number_only")
    price_in_words = fields.Char(compute="_get_price_in_words")
    invoices_count = fields.Integer(compute="_get_invoices_count")    
account_invoice()


class invoice_line(models.Model):
    def _get_line_tax(self):
        for row in self:
            row.line_tax_amount = 0
            for line in row.invoice_line_tax_ids:
                row.line_tax_amount += line.amount

    def _get_tax_total(self):
        for row in self:
            row.line_tax_total = row.invoice_id.currency_id.round( row.price_unit * (1 - (row.discount or 0.0) / 100.0) * row.quantity - row.price_subtotal)


    def _get_subtotal_with_tax(self):
        for row in self:
            row.line_price_subtotal_with_tax = row.invoice_id.currency_id.round( row.price_unit * (1 - (row.discount or 0.0) / 100.0) * row.quantity)

    _name = 'account.invoice.line'
    _inherit = 'account.invoice.line'
#     _columns = {
#         'line_tax_amount': fields.function(_get_line_tax, type='float'),
#         'line_tax_total': fields.function(_get_tax_total, type='float'),
#         'line_price_subtotal_with_tax': fields.function(_get_subtotal_with_tax, type='float'),
#     }
    line_tax_amount = fields.Float(compute="_get__get_line_taxinvoices_count")
    line_tax_total = fields.Float(compute="_get_tax_total")
    line_price_subtotal_with_tax = fields.Float(compute="_get_subtotal_with_tax")      