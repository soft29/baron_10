# -*- coding: utf-8 -*-

import time
from odoo.report import report_sxw
from odoo import models, fields
from odoo.addons.jasper_reports import numeral
from odoo.tools.translate import _


class torg_form(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(torg_form, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({'time': time})

class account_invoice(models.Model):
    def _get_number_only(self):
        for row in self:
            if not row.number:
                raise Exception(_('Error!'), _('You must confirm invoice!'))            
            seq_id = self.env['ir.sequence'].search([('code', '=', 'sale.order')], limit=1)
            #rec = self.env['ir.sequence'].browse(seq_id)
            sequence = seq_id.read(['padding', 'active'])[0]
            if sequence and sequence.get('active'):
                padding = sequence.get('padding')
                padding = 0 - int(padding)
                row.number_only = row.number[padding:].lstrip('0')

    def _get_pos_in_words(self):
        for row in self:
            row.pos_in_words = numeral.in_words(len(row.invoice_line_ids))


    def _get_price_in_words(self):
        for row in self:
            rubles = numeral.rubles(int(row.amount_total))
            copek_tmp = round(row.amount_total - int(row.amount_total))
            copek = numeral.choose_plural(int(copek_tmp), (u"копейка", u"копейки", u"копеек"))
            row.price_in_words = ("%s %02d %s")%(rubles, copek_tmp, copek)

    def _get_invoices_count(self):
        for row in self:
            row.invoices_count = len(row.invoice_line_ids)    

    def _weight_nett_in_words(self):
        for invoice in self:
            weight = 0

            for line in invoice.invoice_line_ids:
                if line.product_id.weight:
                    weight += line.product_id.weight*line.quantity

            if weight:
                invoice.weight_nett_in_words = numeral.in_words(weight)
            else:
                invoice.weight_nett_in_words  = ""

    def _weight_brutt_in_words(self):
        for invoice in self:
            weight = 0
            for line in invoice.invoice_line_ids:
                if line.product_id.weight:
                    weight += line.product_id.weight*line.quantity
            if weight:
                invoice.weight_brutt_in_words = numeral.in_words(weight)
            else:
                invoice.weight_brutt_in_words = ""

    def _get_origin_number(self):
        for invoice in self:
            if invoice.partner_id.contract_num and invoice.partner_id.contract_date:
                invoice.origin_number = invoice.partner_id.contract_num
            elif invoice.origin:
                so_obj = self.env['sale.order']
                order_id = so_obj.search([('name', '=', invoice.origin)], limit=1)
                if order_id:
                    #order_id = order_id[0]
                    order = order_id

                    seq_id = self.env['ir.sequence'].search([('code', '=', 'sale.order')])
                    sequence = seq_id.read(['padding', 'active'])[0]
                    if sequence and sequence.get('active'):
                        padding = sequence.get('padding')
                        padding = 0 - int(padding)
                        invoice.origin_number = order.name[padding:].lstrip('0')
                    else:
                        invoice.origin_number = ""
            else:
                invoice.origin_number = ""

    def _get_origin_date(self):
        for invoice in self:
            if invoice.partner_id.contract_date and invoice.partner_id.contract_num:
                invoice.origin_date = invoice.partner_id.contract_date
            elif invoice.origin:
                so_obj = self.env['sale.order']
                order_id = so_obj.search([('name', '=', invoice.origin)], limit=1)
                if order_id:
                    #order_id = order_id[0]
                    invoice.origin_date = order_id.date_order
            else:
                invoice.origin_date = ""

    def _get_origin_type(self):
        for invoice in self:
            if invoice.partner_id.contract_num:
                if invoice.partner_id.factoring and invoice.partner_id.factoring_conditions:
                    contract = "Договор поставки с отсрочкой платежа"
                else:
                    contract = "Договор"
                invoice.origin_type = contract
            elif invoice.origin:
                invoice.origin_type = "Заказ"
            else:
                invoice.origin_type = ""

    def _format_inn_kpp(self, inn, kpp):
        res = ""
        if inn and kpp:
            res = u"ИНН/КПП %s/%s" % (inn, kpp)
        elif inn:
            res = u"ИНН %s" % inn
        elif kpp:
            res = u"KPP %s" % kpp
        return res

    def get_partner_info(self):
        return super(account_invoice, self).get_partner_info()

    def _get_fullinfo(self, field, invoice, partner):
        acc_number = None
        bank_name = None
        bank_acc_corr = None
        bank_bic = None

        phone = u"тел.: " + partner.phone if partner.phone else None
        if not phone and partner.parent_id:
            phone = u"тел.: " + partner.parent_id.phone if partner.parent_id.phone else None

        if partner.bank_ids:
            bank = partner.bank_ids[0]
        elif partner.parent_id and partner.parent_id.bank_ids:
            bank = partner.parent_id.bank_ids[0]
        else:
            bank = None

        if bank:
            acc_number = u"р/сч " + bank.acc_number if bank.acc_number else None
            bank_name = u"банк " + bank.bank_name if bank.bank_name else None
            bank_acc_corr = u"корр. счет " + bank.bank_acc_corr if bank.bank_acc_corr else None
            bank_bic = u"БИК " + bank.bank_bic if bank.bank_bic else None

        name = invoice[field[0] + '_name']
        innkpp = invoice[field[0] + '_innkpp']
        address = invoice[field[0] + '_address']

        values = [
            name,
            innkpp,
            address,
            phone,
            acc_number,
            bank_name,
            bank_acc_corr,
            bank_bic
        ]

        values = filter(bool, values)
        values = filter(None, values)

        return ', '.join(values) if values else ""

    def _format_header(self):
        for invoice in self:
            invoice.torg12_header = invoice.partner_id.factoring and invoice.partner_id.factoring_conditions or ''

    _name = 'account.invoice'
    _inherit = 'account.invoice'
#     _columns = {
#         'number_only': fields.function(_get_number_only, type='char'),
#         'price_in_words': fields.function(_get_price_in_words, type='char'),
#         'pos_in_words': fields.function(_get_pos_in_words, type='char'),
#         'invoices_count': fields.function(_get_invoices_count, type='integer'),
#         'weight_nett_in_words': fields.function(_weight_nett_in_words, type='char', store=False),
#         'weight_brutt_in_words': fields.function(_weight_brutt_in_words, type='char', store=False),
#         'origin_number': fields.function(_get_origin_number, type='char', store=False),
#         'origin_date': fields.function(_get_origin_date, type='date', store=False),
#         'origin_type': fields.function(_get_origin_type, type='char', store=False),
#         'shipping_innkpp': fields.function(get_partner_info, type='char', store=False, multi='partner_info'),
#         'company_partner_name': fields.function(get_partner_info, type='char', store=False, multi='partner_info'),
#         'company_fullinfo': fields.function(get_partner_info, type='char', store=False, multi='partner_info'),
#         'partner_fullinfo': fields.function(get_partner_info, type='char', store=False, multi='partner_info'),
#         'shipping_fullinfo': fields.function(get_partner_info, type='char', store=False, multi='partner_info'),
#         'invoice_fullinfo': fields.function(get_partner_info, type='char', store=False, multi='partner_info'),
#         'torg12_header': fields.function(_format_header, type='char'),
#     }
    
    number_only = fields.Char(compute="_get_number_only")
    price_in_words = fields.Char(compute="_get_price_in_words")   
    pos_in_words = fields.Char(compute="_get_pos_in_words")   
    invoices_count = fields.Integer(compute="_get_invoices_count")   
    weight_nett_in_words = fields.Char(compute="_weight_nett_in_words")   
    weight_brutt_in_words = fields.Char(compute="_weight_brutt_in_words")   
    origin_number = fields.Char(compute="_get_origin_number")   
    origin_date = fields.Date(compute="_get_origin_date")   
    origin_type = fields.Char(compute="_get_origin_type")
    shipping_innkpp = fields.Char(compute="get_partner_info")
    company_partner_name = fields.Char(compute="get_partner_info") 
    company_fullinfo = fields.Char(compute="get_partner_info") 
    partner_fullinfo = fields.Char(compute="get_partner_info") 
    shipping_fullinfo = fields.Char(compute="get_partner_info") 
    invoice_fullinfo = fields.Char(compute="get_partner_info") 
    torg12_header = fields.Char(compute="_format_header")         
        
account_invoice()


class invoice_line(models.Model):
    def _get_line_tax(self):
        for row in self:
            row.line_tax_amount = 0
            for line in row.invoice_line_tax_ids:
                row.line_tax_amount += line.amount

    def _get_tax_total(self):
        for row in self:
            row.line_tax_total = row.invoice_id.currency_id.round( row.price_unit * (1 - (row.discount or 0.0) / 100.0) * row.quantity - row.price_subtotal)


    def _get_subtotal_with_tax(self):
        for row in self:
            row.line_price_subtotal_with_tax = row.invoice_id.currency_id.round( row.price_unit * (1 - (row.discount or 0.0) / 100.0) * row.quantity)


    _name = 'account.invoice.line'
    _inherit = 'account.invoice.line'
#     _columns = {
#         'line_tax_amount': fields.function(_get_line_tax, type='float'),
#         'line_tax_total': fields.function(_get_tax_total, type='float'),
#         'line_price_subtotal_with_tax': fields.function(_get_subtotal_with_tax, type='float'),
#     }
    
    line_tax_amount = fields.Float(compute="_get_line_tax")
    line_tax_total = fields.Float(compute="_get_tax_total")
    line_price_subtotal_with_tax = fields.Float(compute="_get_subtotal_with_tax")      


class res_partner(models.Model):
    _inherit = 'res.partner'
#     _columns = {
#         'factoring': fields.boolean('Factoring'),
#         'factoring_conditions': fields.text('Factoring conditions'),
#     }
    
    factoring = fields.Boolean(string="Factoring")
    factoring_conditions = fields.Text(string="Factoring conditions")    
