# -*- coding: utf-8 -*-
from odoo import models, fields
from odoo import api
import odoo.addons.decimal_precision as dp


class sale_order_line(models.Model):
    _name = 'sale.order.line'
    _inherit = 'sale.order.line'

    def _amount_line(self):
        tax_obj = self.env['account.tax']
        cur_obj = self.env['res.currency']
        for line in self:
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            
            #soft29
            if len(self) == 0:
                company_id = self.env.user.company_id
            else:
                company_id = self[0].company_id
            currency = company_id.currency_id              
            t = tax_obj.browse(line.tax_id.id)
            
            taxes = t.compute_all(price, currency, line.product_uom_qty, line.product_id, line.order_id.partner_id)
            cur = line.order_id.pricelist_id.currency_id
            
            c = cur_obj.browse(cur.id)
            line.price_subtotal = c.round(taxes['total_included'])

#     _columns = {
#         'price_subtotal': fields.function(_amount_line, string='Subtotal', digits_compute=dp.get_precision('Account')),
#     }
    
    price_subtotal = fields.Integer(compute="_amount_line", string='Subtotal', digits_compute=dp.get_precision('Account'))
sale_order_line()

#soft29
# class sale_order(osv.osv):
#     _name = 'sale.order'
#     _inherit = 'sale.order'
# 
#     def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
#         cur_obj = self.pool.get('res.currency')
#         res = {}
#         for order in self.browse(cr, uid, ids, context=context):
#             res[order.id] = {
#                 'amount_untaxed': 0.0,
#                 'amount_tax': 0.0,
#                 'amount_total': 0.0,
#             }
#             val = val1 = 0.0
#             cur = order.pricelist_id.currency_id
#             for line in order.order_line:
#                 tax = self._amount_line_tax(cr, uid, line, context=context)
#                 val1 += line.price_subtotal - tax
#                 val += tax
#             res[order.id]['amount_tax'] = cur_obj.round(cr, uid, cur, val)
#             res[order.id]['amount_untaxed'] = cur_obj.round(cr, uid, cur, val1)
#             res[order.id]['amount_total'] = res[order.id]['amount_untaxed'] + res[order.id]['amount_tax']
# 
#         return res
# 
#     def _get_order(self, cr, uid, ids, context=None):
#         result = {}
#         for line in self.pool.get('sale.order.line').browse(cr, uid, ids, context=context):
#             result[line.order_id.id] = True
#         return result.keys()
# 
#     _columns = {
#         'amount_untaxed': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Untaxed Amount',
#             store={
#                 'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
#                 'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
#             },
#             multi='sums', help="The amount without tax.", track_visibility='always'),
#         'amount_tax': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Taxes',
#             store={
#                 'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
#                 'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
#             },
#             multi='sums', help="The tax amount."),
#         'amount_total': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Total',
#             store={
#                 'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
#                 'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
#             },
#             multi='sums', help="The total amount."),
#     }
# sale_order()