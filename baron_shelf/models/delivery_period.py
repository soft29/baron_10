# coding: utf-8
from odoo import models, fields, api, _


class delivery_period(models.Model):
    _name = "delivery.period"
    _description = u"Периоды доставки"

    name = fields.Char(string=u"Название")


class Order(models.Model):
    _inherit = 'sale.order'

    delivery_period_id = fields.Many2one(string=u'Период доставки',
                                         comodel_name='delivery.period',
                                         track_visibility='onchange')
