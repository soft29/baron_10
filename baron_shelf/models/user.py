# coding: utf-8
from odoo import models, fields, api, _


class baron_groups(models.Model):
    _name = "baron_shelf.baron_groups"

    name = fields.Char(string='Name', required=True)
    minimal_order_price = fields.Float(string=u"Минимальная сумма заказа")
    user_ids = fields.Many2many('res.partner', 'user_group_table_p', 'group_id_rel_p', 'user_id_rel_p', string='Users')
    shelf_ids = fields.Many2many('baron_shelf.shelf', 'shelf_group_table', 'group_rel', 'shelf_rel', string='Shelves')
    delivery_period_ids = fields.Many2many(
        "delivery.period",
        "baron_groups_delivery_attributes_rel",
        "baron_groups_id",
        "delivery_period_id",
        string=u"Периоды доставки",
    )


class res_partner(models.Model):
    _inherit = "res.partner"

    @api.one
    def change_gorups_baron(self):
        products = self.env['product.template'].sudo().search([])
        for product in products:
            product.sudo().get_users()

    group_baron = fields.Many2many('baron_shelf.baron_groups', 'user_group_table_p', 'user_id_rel_p',
                                   'group_id_rel_p', string='Product groups', inverse='change_gorups_baron', copy=True)
