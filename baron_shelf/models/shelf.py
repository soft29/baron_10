#coding: utf-8
from odoo import models, fields, api, _

class shelf(models.Model):
    _name = "baron_shelf.shelf"

    name = fields.Char(string='Name',required=True)

    @api.one
    def change_gorups_baron(self):
        products = self.env['product.template'].sudo().search([])
        for product in products:
            product.sudo().get_users()

    group_ids = fields.Many2many('baron_shelf.baron_groups',
                                 'shelf_group_table',
                                 'shelf_rel',
                                 'group_rel',
                                 string='User groups',
                                 inverse='change_gorups_baron')
    product_ids = fields.Many2many('product.template',
                                   'shelf_product_x',
                                   'shelf_rel1x',
                                   'shelf_rel2x',
                                   string='Products')
    website_published = fields.Boolean('Products are available in the website', copy=False)


class product_template(models.Model):
    _inherit = "product.template"

    shelf_ids = fields.Many2many('baron_shelf.shelf',
                                 'shelf_product_x',
                                 'shelf_rel2x',
                                 'shelf_rel1x',
                                 string='Shelves')

    user_baron_ids = fields.Many2many('res.partner', compute='get_users', store=True)

    @api.one
    @api.depends('shelf_ids.group_ids.user_ids',
                 'shelf_ids.website_published')
    def get_users(self):
        partners = self.shelf_ids.filtered(lambda s: s.website_published).mapped('group_ids.user_ids')
        self.user_baron_ids = [(6,0, partners.ids or [3])]
