# -*- coding: utf-8 -*-
{
    'name': 'Baron Product Shelves',
    'version': '1.0',
    'category': 'Website',
    'summary': 'Products shelves visibility by user groups',
    'description': '''
Products shelves visibility by user groups
Delivery period for checkout and sale order
    ''',
    'auto_install': False,
    'application': True,
    'author': 'IT Libertas, SUVIT LLC',
    'website': 'http://itlibertas.com,https://suvit.ru',
    'depends': [
        'website_sale',
    ],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',

        'views/user_shelves.xml',
        'views/user_groups.xml',
        'views/delivery_period.xml',
        'views/sale_order.xml',

        'data/data.xml',
    ],
    'qweb': [
    ],
    'js': [
    ],
    'demo': [
    ],
    'test': [
    ],
    'license': 'AGPL-3',
    'images': ['static/description/main.png'],
    'update_xml': [],
    'application': True,
    'installable': True,
    'private_category': True,
    'external_dependencies': {
    },
}
