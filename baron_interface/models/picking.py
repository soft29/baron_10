# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class stock_picking(models.Model):
    _inherit = "stock.picking"

     
    @api.one
    @api.depends('partner_id.street','partner_id.street2','partner_id.city','partner_id.state_id','partner_id.zip','partner_id.country_id',)
    def get_address(self):
    	self.delivery_address = self.partner_id.contact_address

    delivery_address = fields.Char(string='Address',compute='get_address',store=True)
    