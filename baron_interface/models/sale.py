# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class sale_order(models.Model):
    _inherit = 'sale.order'

    @api.one
    @api.depends('invoice_ids.state', 'state')
    def _compute_color(self):
        if not self.invoice_ids:
            self.invoice_color = 'white'
            return
        not_paid = False
        for invoice in self.invoice_ids:
            if invoice.state == "cancel":
                self.invoice_color = 'red'
                return
            if invoice.state != "paid":
                not_paid = True
        if not_paid:
            self.invoice_color = 'blue'
            return
        self.invoice_color = 'green'

    @api.one
    @api.depends('picking_ids.state', 'state')
    def _compute_pickings(self):
        if not self.picking_ids:
            self.pickings = '0/0'
            return
        completed = 0
        total = len(self.picking_ids)
        for picking in self.picking_ids:
            if picking.state == 'done':
                completed += 1
        self.pickings = '{completed}/{total}'.format(completed=completed, total=total)


    invoice_color = fields.Selection([
        ('white', ''),
        ('red', ''),
        ('green', ''),
        ('blue', '',),],
        string=u'Состояние счетов',
        compute=_compute_color,
        #store=True,
        help=u'-Белый индикатор - счета отсутствуют\
        \n-Зелёный индикатор - все счета оплачены\
        \n-Синий индикатор - хотя бы один счёт не оплачен\
        \n-Красный индикатор - хотя бы один счёт отменён',
    )

    pickings = fields.Char(
        string=u"Доставки",
        compute=_compute_pickings,
        help=u'-Белый индикатор - счета отсутствуют\
        \n-Зелёный индикатор - все счета оплачены\
        \n-Синий индикатор - хотя бы один счёт не оплачен\
        \n-Красный индикатор - хотя бы один счёт отменён',
    )
