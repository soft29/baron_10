# -*- coding: utf-8 -*-
{
    'name': 'Baron Interface Modifications',
    'version': '1.0',
    'category': 'Category',
    'summary': 'Baron Interface Modifications',
    'description': '''
Baron Interface Modifications
    ''',
    'auto_install': False,
    'application':True,
        
    'author': 'IT Libertas',
    'website': 'http://itlibertas.net',
    'depends': [
        'delivery',
        'web_tree_dynamic_colored_field',
    ],
    'data': [ 
        'views/picking.xml',
        'views/sale.xml',
        'security/ir.model.access.csv',
            ],
    'qweb': [ 
    
            ],
    'js': [ 

            ],
    'demo': [ 

            ],
    'test': [ 

            ],
    'license': 'AGPL-3',
    'images': ['static/description/main.png'],
    'update_xml': [],
    'application':True,
    'installable': True,
    'private_category':True,
}
