#coding: utf-8
from datetime import datetime, timedelta
import time
from odoo import fields, models
from odoo.tools.translate import _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import odoo.addons.decimal_precision as dp
from odoo import workflow
 
class product_attribute_value(models.Model):
    _inherit = "product.attribute.value"
 
    def get_value_from_price(self):
#         result = dict.fromkeys(self.ids, 0)
#         if not self._context.get('active_id'):
#             return result
        for obj in self:
            for price_id in obj.price_ids:
                if price_id.product_tmpl_id.id == self._context.get('active_id'):
                    obj.price_plus = price_id.price_plus
                    break
#         return result
 
    def get_value_from_price_multiple(self):
#         result = dict.fromkeys(self.ids, 0)
#         if not self._context.get('active_id'):
#             return result
        for obj in self:
            for price_id in obj.price_ids:
                if price_id.product_tmpl_id.id == self._context.get('active_id'):
                    obj.price_multiple = price_id.price_multiple
                    break
#         return result
 
    def set_price_in_line(self):
        context = self._context
        if context is None:
            context = {}
        if 'active_id' not in self._context:
            return None
        p_obj = self.env['product.attribute.price']
        p_ids = p_obj.search([('value_id', '=', self.id), ('product_tmpl_id', '=', context['active_id'])])
        if p_ids:
            p_ids.write({'price_plus': self.price_plus})
        else:
            p_obj.create({
                    'product_tmpl_id': context['active_id'],
                    'value_id': self.id,
                    'price_plus': self.price_plus,
                })
 
    def set_price_in_line_multiple(self):
        context = self._context
        if context is None:
            context = {}
        if 'active_id' not in self._context:
            return None
        p_obj = self.env['product.attribute.price']
        p_ids = p_obj.search([('value_id', '=', self.id), ('product_tmpl_id', '=', context['active_id'])])
        if p_ids:
            p_ids.write({'price_multiple': self.price_multiple})
        else:
            p_obj.create({
                    'product_tmpl_id': context['active_id'],
                    'value_id': self.id,
                    'price_multiple': self.price_multiple,
                })
     
  
    price_plus = fields.Float(compute="get_value_from_price", string='Price Extra', 
                              inverse="set_price_in_line", digits=dp.get_precision('Product Price'))
    price_multiple = fields.Float(compute="get_value_from_price_multiple", string='Coefficient (%)', 
                            inverse="set_price_in_line_multiple", digits=dp.get_precision('Product Price'))    
 

'''
 commented in by soft29
 
'''

# class product_template(osv.osv):
#     _inherit = "product.template"
# 
#     def _price_get(self, cr, uid, products, ptype='list_price', context=None):
#         if context is None:
#             context = {}
# 
#         if 'currency_id' in context:
#             pricetype_obj = self.pool.get('product.price.type')
#             price_type_id = pricetype_obj.search(cr, uid, [('field','=',ptype)])[0]
#             price_type_currency_id = pricetype_obj.browse(cr,uid,price_type_id).currency_id.id
# 
#         res = {}
#         product_uom_obj = self.pool.get('product.uom')
#         for product in products:
#             # standard_price field can only be seen by users in base.group_user
#             # Thus, in order to compute the sale price from the cost price for users not in this group
#             # We fetch the standard price as the superuser
#             if ptype != 'standard_price':
#                 res[product.id] = product[ptype] or 0.0
#             else:
#                 company_id = product.env.user.company_id.id
#                 product = product.with_context(force_company=company_id)
#                 res[product.id] = res[product.id] = product.sudo()[ptype]
#             if ptype == 'list_price':
#                 res[product.id] = product._name == "product.product" and product.lst_price or product.list_price
#             if 'uom' in context:
#                 uom = product.uom_id or product.uos_id
#                 res[product.id] = product_uom_obj._compute_price(cr, uid,
#                         uom.id, res[product.id], context['uom'])
#             # Convert from price_type currency to asked one
#             if 'currency_id' in context:
#                 # Take the price_type currency from the product field
#                 # This is right cause a field cannot be in more than one currency
#                 res[product.id] = self.pool.get('res.currency').compute(cr, uid, price_type_currency_id,
#                     context['currency_id'], res[product.id],context=context)
# 
#         return res