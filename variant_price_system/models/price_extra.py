#coding: utf-8
from odoo import models, fields, api, _

class product_attribute_price(models.Model):
    _inherit = "product.attribute.price"

    price_plus = fields.Float(string='Price Extra')
    price_multiple = fields.Float(string='Coefficient')


class product_product(models.Model):
    _inherit = "product.product"

    @api.one
    def get_real_price(self):
        #to get 100% right ordering
        value_ids = self.env['product.attribute.value'].search([('id','in',self.attribute_value_ids.ids)],order='sequence')
        price = self.list_price
#         if 'uom' in self.env.context:
#             uom = self.uom_id
#             price = self.env['product.uom']._compute_price( price=self.list_price, to_unit=self.env.context['uom'])

        for variant_id in value_ids:
            for price_id in variant_id.price_ids:
                if price_id.product_tmpl_id.id == self.product_tmpl_id.id:
                    price = (price+price_id.price_plus)*(1+price_id.price_multiple/100)

        self.lst_price = price


    lst_price = fields.Float(compute='get_real_price')