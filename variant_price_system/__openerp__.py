# -*- coding: utf-8 -*-
{
    'name': 'Advanced variant prices',
    'version': '1.0',
    'category': 'Sales',
    'summary': 'Variant prices based on extras and coefficients',
    'description': '''
Advanced variant prices
=======================
Product variant prices are calculated based on advanced scheme:
    * Add extra price
    * Use coeffient related to base template price
    ''',
    'price': '100.00',
    'currency': 'USD',
    'auto_install': False,
    'application':True,
        
    'author': 'IT Libertas',
    'website': 'http://itlibertas.net',
    'depends': [
        'product',
    ],
    'data': [ 
        'views/price_extra.xml',
        'security/ir.model.access.csv',
            ],
    'qweb': [ 
    
            ],
    'js': [ 

            ],
    'demo': [ 

            ],
    'test': [ 

            ],
    'license': 'AGPL-3',
    'images': ['static/description/main.png'],
    'update_xml': [],
    'application':True,
    'installable': True,

}
