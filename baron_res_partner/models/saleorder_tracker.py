# -*- coding: utf-8 -*-
import logging
from odoo import api, fields, models
from odoo.http import request
from datetime import datetime
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)

class SaleOrderTracker(models.AbstractModel):
    """
        Use:
        class Order(models.Model):
            _name = 'sale.order'
            _inherit = ['sale.order',
                        'baron.saleorder.tracker']
    """
    _name = 'baron.saleorder.tracker'

    @api.multi
    def write(self, vals):
        res = super(SaleOrderTracker, self).write(vals)
        if self.state in set(['sent', 'manual']):
            id = self.partner_id.id;
            dateNow = datetime.now()
            client = self.env['res.partner'].browse(id).write({'last_sales_order_date': dateNow}) 
        return res
