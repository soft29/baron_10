﻿# -*- coding: utf-8 -*-

#from openerp.osv import fields, osv
from pprint import pprint
#from openerp import fields as f
from odoo import fields as f
from odoo import models

class ResPartner(models.Model):
    _inherit = 'res.partner'
	
    last_sales_order_date = f.Datetime(string=u"Дата последнего заказа")

    def _opportunity_meeting_phonecall_count(self):
	    #soft29: super has no _opportunity_meeting_phonecall_count()
        #result = super(ResPartner, self)._opportunity_meeting_phonecall_count()
        #for partner_id, value_dict in result.items():
        #    partner_obj = self.browse([partner_id])
        #    if partner_obj.is_company:
        #        childs_ids = self.pool.get('res.partner').search([('id', 'child_of', partner_id),('id','!=', partner_id)])
        #        childs_obj = self.pool.get('res.partner').browse(childs_ids)
        #        for partner in childs_obj:
        #            #result[partner_obj.id]['phonecall_count'] += len(partner.phonecall_ids)
        #            result[partner_obj.id]['meeting_count'] += len(partner.meeting_ids)
        #return result
        return []

    def _phonecall_count(self):
        res = dict(map(lambda x: (x,0), self.ids))
        for partner in self.browse():
            operator = 'child_of' if partner.is_company else '='
            partner_ids = self.pool.get('res.partner').search([('id', operator, partner.id)])
            partner_obj = self.pool.get('res.partner').browse(partner_ids)
            for obj in partner_obj:
                res[partner.id] += len(obj.phonecall_ids)
        return res


    def schedule_meeting(self):
        ids = self.ids
        partner_ids = list(ids)
        partner_ids.append(self.env['res.users'].browse().partner_id.id)
        for partner_id in ids:
            partner_obj = self.browse([partner_id])
            if partner_obj.is_company:
                childs_ids = self.pool.get('res.partner').search( [('id', 'child_of', partner_id),('id','!=', partner_id)])
                childs_obj = self.pool.get('res.partner').browse( childs_ids)
                ids += [p.id for p in childs_obj]
        res = self.env['ir.actions.act_window'].for_xml_id('calendar', 'action_calendar_event')
        res['context'] = {
            'search_default_partner_ids': list(ids),
            'default_partner_ids': partner_ids,
        }
        return res

    def _sale_order_count(self):
        res = dict(map(lambda x: (x,0), self.ids))
        try:
            for partner in self.browse():
                res[partner.id] = len(partner.sale_order_ids)
                if partner.is_company:
                    childs_ids = self.pool.get('res.partner').search([('id', 'child_of', partner.id),('id','!=', partner.id)])
                    childs_obj = self.pool.get('res.partner').browse(childs_ids)
                    for contact in childs_obj:
                        res[partner.id] += len(contact.sale_order_ids)
        except:
            pass
        return res

    def _journal_item_count(self):
        result = dict(map(lambda x: (x, {'journal_item_count': 0, 'contracts_count': 0}), self.ids))
        AccountMoveLine = self.env['account.move.line']
        AnalyticAccount = self.env['account.analytic.account']
        for partner_id in self.ids:
            partner_obj = self.browse([partner_id])
            operator = 'child_of' if partner_obj.is_company else '='
            result[partner_id]['journal_item_count'] = AccountMoveLine.search_count( [('partner_id', operator, partner_id)])
            result[partner_id]['contracts_count'] = AnalyticAccount.search_count([('partner_id', operator, partner_id)])
        return result

    def _count_template(self, model_name=None):
        result = dict(map(lambda x: (x, 0), self.ids))
        model_obj = self.env[model_name]
        for partner_id in self.ids:
            partner_obj = self.browse([partner_id])
            operator = 'child_of' if partner_obj.is_company else '='
            result[partner_id] = model_obj.search_count([('partner_id', operator, partner_id)])
        return result

    #soft29: crm_claim was removed from Odoo 10 
    #def _claim_count(self, cr, uid, ids, field_name, arg, context=None):
    #    return self._count_template(cr, uid, ids, field_name, arg, context, 'crm.claim')

    def _issue_count(self):
        return self._count_template('project.issue')

    def _task_count(self):
        return self._count_template('project.task')

    #_columns = {
    #    'opportunity_count': f.function(_opportunity_meeting_phonecall_count, string="Opportunity", type='integer', multi='opp_meet'),
    #    'meeting_count': f.function(_opportunity_meeting_phonecall_count, string="# Meetings", type='integer', multi='opp_meet'),
    #    'phonecall_count': f.function(_phonecall_count, string="Phonecalls", type="integer"),
    #    'sale_order_count': f.function(_sale_order_count, string='# of Sales Order', type='integer'),
    #    'contracts_count': f.function(_journal_item_count, string="Contracts", type='integer', multi="invoice_journal"),
    #    'journal_item_count': f.function(_journal_item_count, string="Journal Items", type="integer", multi="invoice_journal"),
    #    'claim_count': f.function(_claim_count, string='# Claims', type='integer'),
    #    'issue_count': f.function(_issue_count, string='# Issues', type='integer'),
    #    'task_count': f.function(_task_count, string='# Tasks', type='integer'),
    #}
	
    opportunity_count = f.Integer(compute='_opportunity_meeting_phonecall_count', string="Opportunity", multi='opp_meet')	
    meeting_count = f.Integer(compute='_opportunity_meeting_phonecall_count', string="# Meetings", multi='opp_meet')	
    phonecall_count = f.Integer(compute='_phonecall_count', string="Phonecalls")		
    sale_order_count = f.Integer(compute='_sale_order_count', string="# of Sales Order")	
    contracts_count = f.Integer(compute='_journal_item_count', string="Contracts")	
    journal_item_count = f.Integer(compute='_journal_item_count', string="Journal Items")		
    #claim_count = f.Integer(compute='_claim_count', string="# Claims")	
    issue_count = f.Integer(compute='_issue_count', string="# Issues")		
    task_count = f.Integer(compute='_task_count', string="# Tasks")	
	

    def copy(self, cr, uid, id, default=None, context=None):
        if not default:
            default = {}
        default['property_product_pricelist'] = self.browse(cr, uid, id, context=context).property_product_pricelist.id
        return super(res_partner, self).copy(cr, uid, id, default, context=context)
