#coding: utf-8
from datetime import datetime, timedelta
import time
from odoo import fields,models
#from openerp.tools.translate import _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import odoo.addons.decimal_precision as dp
from odoo import workflow


class product_attribute_value(models.Model):
    _inherit = "product.attribute.value"

    def get_value_from_price_pack_true(self):
#         result = dict.fromkeys(self.ids, 0)
#         if not self._context.get('active_id'):
#             return result
        for obj in self:
            for price_id in obj.price_ids:
                if price_id.product_tmpl_id.id == self._context.get('active_id'):
                    obj.pack_true = price_id.pack_true
                    break
#         return result

    def get_value_from_price_multiple_pack_qty(self):
#         result = dict.fromkeys(self.ids, 0)
#         if not self._context.get('active_id'):
#             return result
        for obj in self:
            for price_id in obj.price_ids:
                if price_id.product_tmpl_id.id == self._context.get('active_id'):
                    obj.pack_qty = price_id.pack_qty
                    break
#         return result

    def set_price_in_pack_true(self):
        context = self._context
        if context is None:
            context = {}
        if 'active_id' not in context:
            return None
        p_obj = self.env['product.attribute.price']
        p_ids = p_obj.search([('value_id', '=', self.id), ('product_tmpl_id', '=', context['active_id'])])
        if p_ids:
            p_ids.write({'pack_true': self.pack_true})
        else:
            p_obj.create({
                    'product_tmpl_id': context['active_id'],
                    'value_id': self.id,
                    'pack_true': self.pack_true,
                })

    def set_price_in_line_pack_qty(self):
        context = self._context
        if context is None:
            context = {}
        if 'active_id' not in context:
            return None
        p_obj = self.env['product.attribute.price']
        p_ids = p_obj.search([('value_id', '=', self.id), ('product_tmpl_id', '=', context['active_id'])])
        if p_ids:
            p_ids.write({'pack_qty': self.pack_qty})
        else:
            p_obj.create({
                    'product_tmpl_id': context['active_id'],
                    'value_id': self.id,
                    'pack_qty': self.pack_qty,
                })
    
    pack_true = fields.Boolean(compute="get_value_from_price_pack_true", string='Package', inverse="set_price_in_pack_true")
    pack_qty = fields.Float(compute="get_value_from_price_multiple_pack_qty", string='Quantity in package', 
                            inverse="set_price_in_line_pack_qty", digits=dp.get_precision('Product Price'))
    
#     _columns = {
#         'pack_true': fields.function(get_value_from_price_pack_true, type='boolean', string='Package',
#             fnct_inv=set_price_in_pack_true,),
#         'pack_qty': fields.function(get_value_from_price_multiple_pack_qty, type='float', string='Quantity in package',
#             fnct_inv=set_price_in_line_pack_qty,
#             digits_compute=dp.get_precision('Product Price'),)
#     }
