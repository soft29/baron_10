#coding: utf-8
from odoo import models, fields, api, _

class product_attribute_value(models.Model):
    _inherit = "product.attribute.value"

    @api.one
    @api.constrains('pack_true','pack_qty')
    def _check_qty(self):
        if self.pack_true and self.pack_qty <= 0:
            raise Warning(_('Quantity in pack can not be less than 0!'))

    pack_true = fields.Boolean(string='Package')
    pack_qty = fields.Float(string='Quantity in package')


class product_attribute_price(models.Model):
    _inherit = "product.attribute.price"

    pack_true = fields.Boolean(string='Package')
    pack_qty = fields.Float(string='Quantity in package')


