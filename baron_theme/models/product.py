﻿# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.osv import expression
import re
import random
from itertools import chain


class ProductTemplate(models.Model):
    _inherit = "product.template"

    description = fields.Html(u'Описание', translate=False)

    bestseller_bread = fields.Boolean(u"Хит продаж (выпечка)", default=False)
    bestseller_meat = fields.Boolean(u"Хит продаж (мясо)", default=False)
    bestseller = fields.Selection([
        ('bread', u'Выпечка'),
        ('meat', u'Мясо')], u"Хит продаж", inverse="on_inverse_bestseller")

    @api.one
    def on_inverse_bestseller(self):
        if self.bestseller:
            self.bestseller_bread = \
                True if self.bestseller == 'bread' else False
            self.bestseller_meat = \
                True if self.bestseller == 'meat' else False
        else:
            self.bestseller_bread = False
            self.bestseller_meat = False


class product_product(models.Model):
    _inherit = "product.product"

    description = fields.Html(u'Описание', translate=False)

    @api.multi
    def get_attributes_values(self):
        for product in self:
            # variant = ", ".join([v.name for v in product.attribute_value_ids])
            variant = ''
            for v in product.attribute_value_ids:
                variant += v.name
                for price_id in v.price_ids:
                    if price_id.product_tmpl_id.id == product.product_tmpl_id.id:
                        if price_id.pack_true:
                            variant += ''  #' x ' + ('%.2f' % price_id.pack_qty) + u" шт."
                variant += ', '
            product.attributes_values_string = variant and "(%s)" % (
                variant[:-2]) or ''

    @api.one
    def get_default_pack_qty(self):
        value_ids = self.env['product.attribute.value'].search([('id','in',self.attribute_value_ids.ids)],order='sequence')
        for variant_id in value_ids:
            for price_id in variant_id.price_ids:
                if price_id.product_tmpl_id.id == self.product_tmpl_id.id:
                    if price_id.pack_true:
                        return price_id.pack_qty
        return 0

    @api.one
    def get_uom_qty(self):
        uom_qty = 1
        for v in self.attribute_value_ids:
            for price_id in v.price_ids:
                if price_id.product_tmpl_id.id == self.product_tmpl_id.id:
                    if price_id.pack_true:
                        uom_qty = uom_qty * price_id.pack_qty
        #uom_qty = uom_qty / self.uos_coeff
        return uom_qty

    attributes_values_string = fields.Char(
        string='Attributes values',
        compute=get_attributes_values)

    bestseller_id = fields.Many2one('bestseller.category', u'Категория хита продаж')
    bestseller_ids = fields.Many2many('bestseller.category', 'categ_product_rel', 'categ_id' , 'prod_id', u'Категория хита продаж')

#     def name_search(self, cr, user, name='', args=None, operator='ilike', context=None, limit=100):
#         if not args:
#             args = []
#         if name:
#             positive_operators = ['=', 'ilike', '=ilike', 'like', '=like']
#             ids = []
#             if operator in positive_operators:
#                 ids = self.search(cr, user, [('default_code','=',name)]+ args, limit=limit, context=context)
#                 if not ids:
#                     ids = self.search(cr, user, [('ean13','=',name)]+ args, limit=limit, context=context)
#                 if not ids:
#                     ids = self.search(cr, user, [('attribute_value_ids.name','ilike',name)]+ args, limit=limit, context=context)
#             if not ids and operator not in expression.NEGATIVE_TERM_OPERATORS:
#                 # Do not merge the 2 next lines into one single search, SQL search performance would be abysmal
#                 # on a database with thousands of matching products, due to the huge merge+unique needed for the
#                 # OR operator (and given the fact that the 'name' lookup results come from the ir.translation table
#                 # Performing a quick memory merge of ids in Python will give much better performance
#                 ids = set(self.search(cr, user, args + [('default_code', operator, name)], limit=limit, context=context))
#                 if not limit or len(ids) < limit:
#                     # we may underrun the limit because of dupes in the results, that's fine
#                     limit2 = (limit - len(ids)) if limit else False
#                     ids.update(self.search(cr, user, args + [('name', operator, name), ('id', 'not in', list(ids))], limit=limit2, context=context))
#                 ids = list(ids)
#             elif not ids and operator in expression.NEGATIVE_TERM_OPERATORS:
#                 ids = self.search(cr, user, args + ['&', ('default_code', operator, name), ('name', operator, name)], limit=limit, context=context)
#             if not ids and operator in positive_operators:
#                 ptrn = re.compile('(\[(.*?)\])')
#                 res = ptrn.search(name)
#                 if res:
#                     ids = self.search(cr, user, [('default_code','=', res.group(2))] + args, limit=limit, context=context)
#         else:
#             ids = self.search(cr, user, args, limit=limit, context=context)
#         result = self.name_get(self) #cr, user, ids, context=context
#         return result

    @api.multi
    def price_compute(self, price_type, uom=False, currency=False, company=False):
        # TDE FIXME: delegate to template or not ? fields are reencoded here ...
        # compatibility about context keys used a bit everywhere in the code
        if not uom and self._context.get('uom'):
            uom = self.env['product.uom'].browse(self._context['uom'])
        if not currency and self._context.get('currency'):
            currency = self.env['res.currency'].browse(self._context['currency'])

        products = self
        if price_type == 'standard_price':
            # standard_price field can only be seen by users in base.group_user
            # Thus, in order to compute the sale price from the cost for users not in this group
            # We fetch the standard price as the superuser
            products = self.with_context(force_company=company and company.id or self._context.get('force_company', self.env.user.company_id.id)).sudo()

        prices = dict.fromkeys(self.ids, 0.0)
        for product in products:
            prices[product.id] = product[price_type] or 0.0
            if price_type == 'list_price':
                # the only string changed by soft29
                prices[product.id] = product.lst_price or product.list_price
                #was : prices[product.id] += product.price_extra                

            if uom:
                prices[product.id] = product.uom_id._compute_price(prices[product.id], uom)

            # Convert from current user company currency to asked one
            # This is right cause a field cannot be in more than one currency
            if currency:
                prices[product.id] = product.currency_id.compute(prices[product.id], currency)

        return prices

    def name_get(self): #, cr, user, ids, context=None
        result = super(product_product, self).name_get() #cr, user, ids, context=context

        context = self._context or {}
        # add id to name for bad graph view. graph view has group_by_no_leaf by default
        if not context.get('group_by_no_leaf'):
            return result

        new_results = []
        for item in result:
            new_results.append((item[0], u'%s (id %d)' % (item[1], item[0])))

        return new_results


class Partner(models.Model):
    _inherit = "res.partner"

    def name_get(self): #self, cr, user, ids, context=None
        result = super(Partner, self).name_get() #cr, user, ids, context=context

        context = self._context or {}
        # add id to name for bad graph view. graph view has group_by_no_leaf by default
        if not context.get('group_by_no_leaf'):
            return result

        new_results = []
        for item in result:
            new_results.append((item[0], u'%s (id %d)' % (item[1], item[0])))

        return new_results


class bestseller_category(models.Model):
    _name = 'bestseller.category'
    _description = u"Категории Хитов Продаж"

    @api.multi
    def get_random_bestsellers(self):
        products = self.products_ids
        s = set(product for product in products if product.website_published == True)
        product_ids = random.sample(s, min(len(s), 4))
        return product_ids

    name = fields.Char(u"Название")
    product_ids = fields.One2many('product.product','bestseller_id', u'Продукция')
    products_ids = fields.Many2many('product.product', 'categ_product_rel', 'prod_id' , 'categ_id', u'Продукция')
    sequence = fields.Integer(u'Порядок')
    public = fields.Boolean(u'Опубликовано')

    _order = "sequence, id"


# class ProductPricelist(models.Model):
#     _inherit = "product.pricelist"
#
#     @api.model
#     def _price_rule_get_multi(self, pricelist, products_by_qty_by_partner):
#         product_uom_obj = self.env['product.uom']
#         res = super(ProductPricelist, self)._price_rule_get_multi(pricelist, products_by_qty_by_partner)
#         products = map(lambda x: x[0], products_by_qty_by_partner)
#         is_product_template = products[0]._name == "product.template"
#         if is_product_template:
#             for rec in res:
#                 product = self.env['product.template'].sudo().browse(rec)
#                 res[rec] = (product_uom_obj._compute_price(product.uom_id.id, res[rec][0], product.uom_id.id),res[rec][1])
#         else:
#             for rec in res:
#                 product = self.env['product.product'].sudo().browse(rec)
#                 res[rec] = (product_uom_obj._compute_price(product.uom_id.id, res[rec][0], product.uom_id.id),res[rec][1])
#         return res
#
