# -*- coding: utf-8 -*-
{
    'name': 'Baron theme site',
    'description': 'Theme for Baron Llc',
    'category': 'Theme',
    'version': '1.0',
    'website': '',
    'author': 'IT Libertas, Ilyas',
    'depends': [
        'website_sale',
        'payment_transfer',

        'baron_shop_js',
        'baron_shelf',
    ],
    'external_dependencies': {'python': ['bs4']},
    'data': [
        'security/ir.model.access.csv',

        'views/product_view.xml',

        'views/theme.xml',
        'views/templates/checkout.xml',
        'views/templates/payment.xml',
        'views/templates/confirmation.xml',
    ],
    'images':[
        'static/description/splash_screen.png',
    ],
    'application': True,
}
