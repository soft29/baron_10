# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup

from odoo import SUPERUSER_ID
from odoo import http
from odoo.http import request, Response
from odoo.tools.translate import _

from odoo.addons.web.controllers.main import Home

from odoo.addons.website.models.website import slug
from odoo.addons.website.controllers.main import Website

from odoo.addons.website_crm.controllers.main import WebsiteCrmBackend

from odoo.addons.website_sale.controllers.main import QueryURL
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.website_sale.controllers.main import TableCompute

from odoo.addons.payment_transfer.controllers.main import OgoneController

import logging
_logger = logging.getLogger(__name__)

PPG = 20  # Products Per Page
PPR = 4  # Products Per Row


def drop_suggested_products(products, line_len=4):
    suggested_products = []
    if products:
        if len(products) >= line_len:
            for a in range(0, len(products), line_len):
                suggested_products.append(products[a:a + line_len])
        else:
            suggested_products.append([])
            for product in products:
                suggested_products[0].append(product)
    return suggested_products


def get_pricelist():
    sale_order = request.context.get('sale_order')
    if sale_order:
        pricelist = sale_order.pricelist_id
    else:
        pricelist = request.env.user.partner_id.property_product_pricelist
    return pricelist


class baron_website(Website):

    def get_pricelist(self):
        return get_pricelist()

    def get_attribute_value_ids(self, product):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        currency_obj = pool['res.currency']
        attribute_value_ids = []
        currOrder = request.website.sale_get_order()
        # if coupon (promo) applied
        if currOrder:
            context['pricelist'] = currOrder.pricelist_id
            pricelist_of_order = currOrder.pricelist_id.id        
        visible_attrs = set(l.attribute_id.id
                            for l in product.attribute_line_ids
                            if len(l.value_ids) > 1)
        if request.website.pricelist_id.id != context['pricelist']:
            for p in product.product_variant_ids:
                # if coupon (promo) applied
                if pricelist_of_order:
                    price = pool.get('product.pricelist').price_get(cr, uid, [pricelist_of_order], p.id, 1.0, None, context)[pricelist_of_order]
                else:        
                    website_currency_id = request.website.currency_id.id
                    currency_id = self.get_pricelist().currency_id.id                             
                    price = currency_obj.compute(cr, uid, website_currency_id, currency_id, p.lst_price)
                attribute_value_ids.append(
                    [p.id, [v.id for v in p.attribute_value_ids if v.attribute_id.id in visible_attrs], p.price, price])
        else:
            attribute_value_ids = [[p.id, [v.id for v in p.attribute_value_ids if v.attribute_id.id in visible_attrs], p.price, p.lst_price]
                                   for p in product.product_variant_ids]
        return attribute_value_ids

    def get_product_template_with_context(self, product=None):
        return product and product.product_tmpl_id or False

    def round_value(self, value):
        res = 0
        if value:
            res = round(value, 2)
        return res

    def get_default_product_for_price(self, product):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        products = product.product_variant_ids.ids
        default_product = False
        if product.attribute_line_ids and product.attribute_line_ids[0].value_ids:
            attrs_vals = []
            for attr in product.attribute_line_ids:
                for val in attr.value_ids:
                    res_ids = pool.get('product.attribute.value').search(
                        cr, uid, [('id', 'in', val.ids)], order="sequence")
                    res = pool.get('product.attribute.value').browse(
                        cr, uid, res_ids)
                    attrs_vals.append(res[0])
            for product in product.product_variant_ids:
                if attrs_vals == product.attribute_value_ids.ids:
                    default_product = product
            if not default_product:
                value_ids = product.attribute_line_ids[0].value_ids.ids
                res_ids = pool.get('product.attribute.value').search(
                    cr, uid, [('id', 'in', value_ids)], order="sequence")
                res = pool.get('product.attribute.value').browse(
                    cr, uid, res_ids)
                for prod in res[0].product_ids:
                    if prod.id in products:
                        default_product = prod
                        break
        else:
            default_product = pool.get('product.product').browse(
                cr, uid, [sorted(products)[0]], context)
        return default_product

    def get_website_price(self, product, prices_data):
        if prices_data and product:
            for line in prices_data:
                if line[0] == product.id:
                    return line[2]
        return 0

    def format_lang(self, value):
        res = 1
        if value:
            digits = 2
            lang = 'ru_RU'
            lang_objs = request.env['res.lang'].search([('code', '=', lang)])
            if not lang_objs:
                lang_objs = request.env['res.lang'].search([('code', '=', 'en_EN')])
            lang_obj = request.env['res.lang'].browse([lang_objs[0]])

            res = lang_obj.format('%.' + str(digits) + 'f', value, grouping=True)
        return res

    @http.route('/page/<page:page>', type='http', auth="public", website=True)
    def page(self, page, **opt):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        keep = QueryURL('/page')
        context = dict(request.env.context)

        if not context.get('pricelist'):
            pricelist = request.website.get_current_pricelist()
            context['pricelist'] = pricelist.id
        else:
            pricelist = pool.get('product.pricelist').browse(cr, uid, context['pricelist'], context)

        values = {
            'path': page,
            'keep': keep,
            'get_attribute_value_ids': self.get_attribute_value_ids,
            'pricelist': pricelist,
            'default_product': self.get_default_product_for_price,
            'format_lang': self.format_lang,
            'round': self.round_value,
            'get_website_price': self.get_website_price,
            'get_product_template_with_context': self.get_product_template_with_context,
        }
        # /page/website.XXX --> /page/XXX
        if page.startswith('website.'):
            return request.redirect('/page/' + page[8:], code=301)
        elif '.' not in page:
            page = 'website.%s' % page

        try:
            request.website.get_template(page)
        except ValueError, e:
            # page not found
            if request.website.is_publisher():
                page = 'website.page_404'
            else:
                return request.registry['ir.http']._handle_exception(e, 404)

        return request.render(page, values)


class baron_website_sale(WebsiteSale):

# soft29: this mthd was fixed in v10 of odoo, no customizations needed
#     def get_attribute_value_ids(self, product):
#         """ list of selectable attributes of a product
# 
#         :return: list of product variant description
#            (variant id, [visible attribute ids], variant price, variant sale price)
#         """
#         # product attributes with at least two choices
#         quantity = product._context.get('quantity') or 1
#         product = product.with_context(quantity=quantity)
# 
#         visible_attrs_ids = product.attribute_line_ids.filtered(lambda l: len(l.value_ids) > 1).mapped('attribute_id').ids
#         to_currency = request.website.get_current_pricelist().currency_id
#         attribute_value_ids = []
#         for variant in product.product_variant_ids:
#             if to_currency != product.currency_id:
#                 price = variant.currency_id.compute(variant.website_public_price, to_currency) / quantity
#             else:
#                 price = variant.website_public_price / quantity
#             visible_attribute_ids = [v.id for v in variant.attribute_value_ids if v.attribute_id.id in visible_attrs_ids]
#             
#             attribute_value_ids.append([variant.id, visible_attribute_ids, variant.website_price, price])
#         return attribute_value_ids

    def get_default_product_for_price(self, product):
        products = product.product_variant_ids.ids
        default_product = False
        if product.attribute_line_ids and product.attribute_line_ids[0].value_ids:
            attrs_vals = []
            for attr in product.attribute_line_ids:
                for val in attr.value_ids:
                    res_ids = request.env['product.attribute.value'].search(
                        [('id', 'in', val.ids)], order="sequence")
                    res = request.env['product.attribute.value'].browse(
                        res_ids)
                    attrs_vals.append(res[0])
            att_ids = [r.id for r in attrs_vals]
            for product in product.product_variant_ids:
                if att_ids == product.attribute_value_ids.ids:
                    default_product = product
            if not default_product:
                value_ids = product.attribute_line_ids[0].value_ids.ids
                result = request.env['product.attribute.value'].search(
                    [('id', 'in', value_ids)], order="sequence", limit=1)

                for prod in result.product_ids:
                    if prod.id in products:
                        default_product = prod
                        break
        else:
            default_product = request.env['product.product'].browse([sorted(products)[0]])
        return default_product

    def get_website_price(self, product, prices_data):
        if prices_data and product:
            for line in prices_data:
                if line[0] == product.id:
                    return line[2]
        return 0

    def format_lang(self, value):
        res = 1
        if value:
            digits = 2
            lang = 'ru_RU'
            lang_objs = request.env['res.lang'].search([('code', '=', lang)])
            if not lang_objs:
                lang_objs = request.env['res.lang'].search([('code', '=', 'en_EN')])
            lang_obj = request.env['res.lang'].browse([lang_objs[0]])

            res = lang_obj.format('%.' + str(digits) + 'f', value
                                  #, grouping=True
                                  )
        return res

    def round_value(self, value):
        res = 0
        if value:
            res = round(value, 2)
        return res

    def get_minimal_order_price(self):
        groups = request.env['res.users'].browse(request.env.user.id).group_baron
        if groups:
            minimal_order_price = min(groups, key=lambda g: g.minimal_order_price).minimal_order_price
        else:
            minimal_order_price = 0
        return minimal_order_price


    def _get_mandatory_billing_fields(self):
        return ["name", 
                #"email", 
                "street", "city", 
                #"country_id"
                ]
    

    def checkout_values(self, **kw):
        values = super(baron_website_sale, self).checkout_values(**kw)
        values['delivery_periods'] = request.env.user.group_baron.mapped('delivery_period_ids')
 
#         def_country = values['countries'][0].id
#         values['checkout'].setdefault('country_id', def_country)
#         values['checkout'].setdefault('shipping_country_id', def_country)
#        values['checkout'].setdefault('sms_track', request.env.user.sms_track)
#         for shipping in values['shippings']:
#             if not shipping.country_id:
#                 shipping.country_id = def_country
# by soft29
#         if isinstance(data, dict):
#             # POST
#             for field in ['delivery_period_id', 'comment', 'sms_track']:
#                 values['checkout'][field] = data.get(field)
 
        return values

    def checkout_form_save(self, checkout):
        #super(baron_website_sale, self)._checkout_form_save(checkout)

        order = request.website.sale_get_order()
        order.write({'delivery_period_id': checkout.get("delivery_period_id", False),
                     'note': (order.note or '') + checkout.get('note', ''),
                     'sms_track': checkout.get('sms_track')})

    @http.route(['/shop/confirm_order'], type='http', auth="public", website=True, csrf=False)
    def confirm_order(self, **post):
        response = super(baron_website_sale, self).confirm_order(**post)
        
        # custom
        values = self.checkout_values()
        custom_vals = {}
        if isinstance(post, dict):
            for field in ['delivery_period_id', 'note', 'sms_track']:
                custom_vals[field] = post.get(field)
        #self.checkout_form_save(values["order"])
        order = values["order"]
        order.write(custom_vals)

        # if redirect to payment, skip it, redirect to confirmation
        if not (isinstance(response, Response) and response.status_code == 302 and response.location == '/shop/payment'):
            return response

        # first get acquirer
        #order = request.website.sale_get_order()
        acquirer_ids = request.env['payment.acquirer'].search([('website_published', '=', True),
                                                               ('company_id', '=', order.company_id.id)])

        # now always redirect with first acquirer
        if 0 and len(acquirer_ids) != 1:
           return response

        # second create transaction
        self.payment_transaction(acquirer_ids[:1].id)

        # third call transfer feedback that redirect to /shop/payment/validate
        # validate check transaction and redirect to confirmation
        post = dict(reference=order.name,
                    amount=order.amount_total,
                    currency=order.pricelist_id.currency_id.name,
                    return_url='/shop/payment/validate')
        return OgoneController.transfer_form_feedback.im_func(self, **post)

    def get_product_template_with_context(self, product=None):
        if not request.context.get('pricelist'):
            request.context['pricelist'] = int(self.get_pricelist())

        return product and product.product_tmpl_id or False

    @http.route(['/shop/pricelist'], type='http', auth="public", website=True, csrf=False)
    def pricelist(self, promo, **post):
        return super(baron_website_sale, self).pricelist(promo.lower().strip(), **post)

    @http.route(['/shop/cart'], type='http', auth="public", website=True)
    def cart(self, **post):
        order = request.website.sale_get_order()
        if order:
            from_currency = order.company_id.currency_id
            to_currency = order.pricelist_id.currency_id
            compute_currency = lambda price: from_currency.compute(price, to_currency)
        else:
            compute_currency = lambda price: price
            
        keep = QueryURL('/shop')

        values = {
            'website_sale_order': order,
            'compute_currency': compute_currency,
            'suggested_products': [],
            'keep': keep,
            'drop_suggested_products': drop_suggested_products,
            'get_attribute_value_ids': self.get_attribute_value_ids,
#             'pricelist': pricelist,
#             'default_product': self.get_default_product_for_price,
            'format_lang': self.format_lang,
#             'round': self.round_value,
            'minimal_order_price': self.get_minimal_order_price(),
#             'get_product_template_with_context': self.get_product_template_with_context,
#             'get_website_price': self.get_website_price
        }
        if order:
            _order = order
            if not request.env.context.get('pricelist'):
                _order = order.with_context(pricelist=order.pricelist_id.id)
            values['suggested_products'] = _order._cart_accessories()

        if post.get('type') == 'popover':
            # force no-cache so IE11 doesn't cache this XHR
            return request.render("website_sale.cart_popover", values, headers={'Cache-Control': 'no-cache'})

        if post.get('code_not_available'):
            values['code_not_available'] = post.get('code_not_available')

        return request.render("website_sale.cart", values)

    @http.route(['/shop',
                 '/shop/page/<int:page>',
                 '/shop/category/<model("product.public.category"):category>',
                 '/shop/category/<model("product.public.category"):category>/page/<int:page>'
                 ], type='http', auth="public", website=True)
    def shop(self, page=0, category=None, search='', ppg=False, **post):
        '''
             there's a custom rule applied on product.template, disabled:
                  ['&','&',
            ('website_published', '=', True),('sale_ok', '=', True),
            '|','|',('user_baron_ids','=',user.partner_id.parent_id.id),('user_baron_ids','=',user.partner_id.id),('shelf_ids','=',False)
            ]
        
        '''
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry

        if ppg:
            try:
                ppg = int(ppg)
            except ValueError:
                ppg = PPG
            post["ppg"] = ppg
        else:
            ppg = PPG

        domain = request.website.sale_product_domain()
        
        if search:
            for srch in search.split(" "):
                domain += ['|', '|', '|', ('name', 'ilike', srch), ('description', 'ilike', srch),
                           ('description_sale', 'ilike', srch), ('product_variant_ids.default_code', 'ilike', srch)]
        if category:
            domain += [('public_categ_ids', 'child_of', int(category))]

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int, v.split("-")) for v in attrib_list if v]
        attributes_ids = set([v[0] for v in attrib_values])
        attrib_set = set([v[1] for v in attrib_values])

        if attrib_values:
            attrib = None
            ids = []
            for value in attrib_values:
                if not attrib:
                    attrib = value[0]
                    ids.append(value[1])
                elif value[0] == attrib:
                    ids.append(value[1])
                else:
                    domain += [('attribute_line_ids.value_ids', 'in', ids)]
                    attrib = value[0]
                    ids = [value[1]]
            if attrib:
                domain += [('attribute_line_ids.value_ids', 'in', ids)]

        keep = QueryURL('/shop', category=category and int(category),
                        search=search, attrib=attrib_list)
        pricelist_context = dict(request.env.context)
        if not pricelist_context.get('pricelist'):
            pricelist = request.website.get_current_pricelist()
            pricelist_context['pricelist'] = pricelist.id
        else:
            pricelist = request.env['product.pricelist'].browse(pricelist_context['pricelist'])

        url = "/shop"
        if search:
            post["search"] = search
        if category:
            category = pool['product.public.category'].browse(
                cr, uid, int(category), context=context)
            url = "/shop/category/%s" % slug(category)
        Product = request.env['product.template']
        product_count = Product.search_count(domain)
        pager = request.website.pager(
            url=url, total=product_count, page=page, step=PPG, scope=7, url_args=post)
        #product_ids = Product.search(cr, uid, domain, limit=PPG, offset=pager[
        #                                 'offset'], order='website_published desc, website_sequence desc', context=context)
        products = Product.search(domain, limit=ppg, offset=pager['offset'], order=self._get_search_order(post))

        
        categs = request.env['product.public.category'].search([('parent_id', '=', False)])
        
        ProductAttribute = request.env['product.attribute']
        if products:
            # get all products without limit
            selected_products = Product.search(domain, limit=False)
            attributes = ProductAttribute.search([('attribute_line_ids.product_tmpl_id', 'in', selected_products.ids)])
        else:
            attributes = ProductAttribute.browse(attributes_ids)

        from_currency = request.env.user.company_id.currency_id
        to_currency = pricelist.currency_id
        compute_currency = lambda price: from_currency.compute(price, to_currency)

        values = {
            'search': search,
            'category': category,
            'attrib_values': attrib_values,
            'attrib_set': attrib_set,
            'pager': pager,
            'pricelist': pricelist,
            'products': products,
            'bins': TableCompute().process(products, ppg),
            'rows': PPR,
            'categories': categs,
            'attributes': attributes,
            'compute_currency': compute_currency,
            'keep': keep,
            'style_in_product': lambda style, product: style.id in [s.id for s in product.website_style_ids],
            'attrib_encode': lambda attribs: werkzeug.url_encode([('attrib', i) for i in attribs]),
            'get_attribute_value_ids': self.get_attribute_value_ids,
            'default_product': self.get_default_product_for_price,
            'format_lang': self.format_lang,
            'round': self.round_value,
            'get_website_price': self.get_website_price,
        }
        return request.render("website_sale.products", values)

    """@http.route(['/shop/product/<model("product.template"):product>'], type='http', auth="public", website=True)
    def product(self, product, category='', search='', **kwargs):
        product_context = dict(request.env.context,
                               active_id=product.id,
                               partner=request.env.user.partner_id)
        
        cr, uid, context, pool = request.cr, request.uid, request.env.context, request.env
        category_obj = pool['product.public.category']
        template_obj = pool['product.template']

        #context.update(active_id=product.id)

        if category:
            category = category_obj.browse(
                cr, uid, int(category), context=context)

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int, v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])

        keep = QueryURL('/shop', category=category and category.id,
                        search=search, attrib=attrib_list)

        #category_ids = category_obj.search(cr, uid, [], context=context)
        category_list = category_obj.name_get() #cr, uid, category_ids, context=context
        category_list = sorted(category_list, key=lambda category: category[1])

        pricelist = request.website.get_current_pricelist() #self.get_pricelist()

        #from_currency = pool.get('product.price.type')._get_field_currency(
        #    cr, uid, 'list_price', context)
        #to_currency = pricelist.currency_id
        #compute_currency = lambda price: pool['res.currency']._compute(
        #    cr, uid, from_currency, to_currency, price, context=context)
        from_currency = request.env.user.company_id.currency_id
        to_currency = pricelist.currency_id
        compute_currency = lambda price: from_currency.compute(price, to_currency)

        #if not context.get('pricelist'):
        #    context['pricelist'] = int(request.website.get_current_pricelist()) #int(self.get_pricelist())
        #    product = template_obj.browse(
        #        cr, uid, int(product), context=context)
        if not product_context.get('pricelist'):
            product_context['pricelist'] = pricelist.id
            product = product.with_context(product_context)

        values = {
            'search': search,
            'category': category,
            'pricelist': pricelist,
            'attrib_values': attrib_values,
            'compute_currency': compute_currency,
            'attrib_set': attrib_set,
            'keep': keep,
            'category_list': category_list,
            'main_object': product,
            'product': product,
            'get_attribute_value_ids': self.get_attribute_value_ids,
            'default_product': self.get_default_product_for_price,
            'format_lang': self.format_lang,
            'round': self.round_value,
            'get_website_price': self.get_website_price,
        }
        return request.render("website_sale.product", values)"""
    @http.route(['/shop/product/<model("product.template"):product>'], type='http', auth="public", website=True)
    def product(self, product, category='', search='', **kwargs):
        product_context = dict(request.env.context,
                               active_id=product.id,
                               partner=request.env.user.partner_id)
        ProductCategory = request.env['product.public.category']
        Rating = request.env['rating.rating']

        if category:
            category = ProductCategory.browse(int(category)).exists()

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int, v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])

        keep = QueryURL('/shop', category=category and category.id, search=search, attrib=attrib_list)

        categs = ProductCategory.search([('parent_id', '=', False)])

        pricelist = request.website.get_current_pricelist()

        from_currency = request.env.user.company_id.currency_id
        to_currency = pricelist.currency_id
        compute_currency = lambda price: from_currency.compute(price, to_currency)

        # get the rating attached to a mail.message, and the rating stats of the product
        ratings = Rating.search([('message_id', 'in', product.website_message_ids.ids)])
        rating_message_values = dict([(record.message_id.id, record.rating) for record in ratings])
        rating_product = product.rating_get_stats([('website_published', '=', True)])

        if not product_context.get('pricelist'):
            product_context['pricelist'] = pricelist.id
            product = product.with_context(product_context)

        values = {
            'search': search,
            'category': category,
            'pricelist': pricelist,
            'attrib_values': attrib_values,
            'compute_currency': compute_currency,
            'attrib_set': attrib_set,
            'keep': keep,
            'categories': categs,
            'main_object': product,
            'product': product,
            'get_attribute_value_ids': self.get_attribute_value_ids,
            'rating_message_values': rating_message_values,
            'rating_product': rating_product,
            'default_product': self.get_default_product_for_price,
            'format_lang': self.format_lang,
            'round': self.round_value,
            'get_website_price': self.get_website_price
        }
        return request.render("website_sale.product", values)

    ''' 
        called only when value in circle changed (direct set or plus minus) on cart form 
    '''
    @http.route(['/shop/cart/update_json'], type='json', auth="public", methods=['POST'], website=True)
    def cart_update_json(self, product_id, line_id, add_qty=None, set_qty=None, display=True):
        so = request.website.sale_get_order(force_create=1)
        #cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        product = request.env['product.product'].browse(int(product_id))
        mult = self.get_multiplier(product)
        add_uos_qty = None
        set_uos_qty = 0
        if add_qty:
            add_uos_qty = float(add_qty) * mult / 1 #product.uos_coeff
        if set_qty:
            set_uos_qty = float(set_qty) * mult / 1 #product.uos_coeff
        value = super(baron_website_sale, self).cart_update_json(product_id, line_id, add_uos_qty, set_uos_qty, display)
        value['lines_price_subtotal'] = False
        if len(so.order_line) > 0:
            #if value['line_id'] in so.order_line.ids:
            if line_id in so.order_line.ids:
                line_qty = filter(lambda x: x.id == line_id #value['line_id']
                                  , so.order_line)[0].product_uom_qty
                line_qty = line_qty / mult
                value['quantity'] = line_qty * 1 #product.uos_coeff
            subs = dict()
            for so_line in so.order_line:
                line_total = request.website.env['baron_website_tools'].convert_price(so_line.price_subtotal)
                subs.update({so_line.id: line_total})
            value['lines_price_subtotal'] = subs
            value['cart_quantity'] = sum([#r.product_id.uos_coeff*
                                          r.product_uom_qty
                                          /self.get_multiplier(r.product_id) 
                                          for r in so.order_line])
        value['baron_theme.minimal_total_alert'] = request.env['ir.ui.view'].render_template(
            "baron_theme.minimal_total_alert", {
                # 'website_sale_order': so,
                'minimal_order_price': self.get_minimal_order_price(),
                'format_lang': self.format_lang,
            }
        )
        return value

    def get_multiplier(self, product):
        res = 1
        for v in product.attribute_value_ids:
            for price_id in v.price_ids:
                if price_id.product_tmpl_id.id == product.product_tmpl_id.id:
                    if price_id.pack_true:
                        res *= price_id.pack_qty
        return res

#     @http.route(['/shop/cart/update'], type='http', auth="public", methods=['POST'], website=True)
#     def cart_update(self, product_id, add_qty=1, set_qty=0, **kw):
#         cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
#         product = registry.get('product.product').browse(cr, uid, int(product_id))
#         mult = self.get_multiplier(product)
#         add_uos_qty = float(add_qty or 0) * mult / product.uos_coeff
#         set_uos_qty = float(set_qty or 0) * mult / product.uos_coeff
#         kw['qty_uos'] = 1
#         value = super(baron_website_sale, self).cart_update(product_id, add_uos_qty, set_uos_qty, **kw)
#         so = request.website.sale_get_order()
#         partner = registry.get('res.users').browse(cr, SUPERUSER_ID, uid, context=context).partner_id
#         pricelist = partner.property_product_pricelist
#         back_to_product_href = "/shop/product/" + slug(product.product_tmpl_id)
#         so.cart_quantity = sum(
#             [r.product_id.uos_coeff * r.product_uom_qty / self.get_multiplier(r.product_id) for r in
#              so.order_line])
#         so.cart_uos_qty = so.cart_quantity
#         return request.redirect(back_to_product_href)
        #return "updated"
        
    @http.route(['/shop/cart/update'], type='http', auth="public", methods=['POST'], website=True, csrf=False)
    def cart_update(self, product_id, add_qty=1, set_qty=0, **kw):
        product = request.env['product.product'].browse(int(product_id))
        mult = self.get_multiplier(product)
        add_qty = float(add_qty or 0) * mult #/ product.uos_coeff
        set_qty = float(set_qty or 0) * mult #/ product.uos_coeff        
        super(baron_website_sale, self).cart_update(product_id, add_qty, set_qty, **kw)
        so = request.website.sale_get_order()
        so.cart_quantity = sum(
            [r.product_uom_qty / self.get_multiplier(r.product_id) 
             for r in so.order_line])
        so.cart_uos_qty = so.cart_quantity        
        product = request.env['product.product'].browse(int(product_id))
        back_to_product_href = "/shop/product/" + slug(product.product_tmpl_id)
        return request.redirect(back_to_product_href)        

    @http.route('/shop/properties', type='json', auth="public", website=True)
    def pyth_met(self, *args, **kwargs):
        res = {'properties': ''}

        if kwargs.get('prod_id'):
            prod = request.env['product.product'].browse(int(kwargs['prod_id']))
        else:
            prod = request.env['product.product']

        if kwargs.get('value_id'):
            val = request.env['product.attribute.value'].browse(int(kwargs['value_id']))
        elif kwargs.get('value_name'):
            val = request.env['product.attribute.value'].search([('name', '=', kwargs['value_name'].strip())],
                                                                 limit=1)
        else:
            val = request.env['product.attribute.value']

        res['prod_property'] = prod.hlebproperty_id.description or ''
        res['prod_property_caption'] = prod.hlebproperty_id.caption or ''

        res['val_property'] = val.hlebproperty_id.description or ''
        res['val_property_caption'] = val.hlebproperty_id.caption or ''

        return res
