odoo.define('baron_theme.variants', function(require) {
"use strict"; 

var ajax = require('web.ajax');

$(document).ready(function () {
	
	var variant_ids; 
	
	function getFirstProd() {
        var $ul = $('div.js_add_cart_variants:first');
        variant_ids = $ul.data("attribute_value_ids");
        // variant_ids when in cart
        if (!variant_ids) {
        	return 0;
        }
        return variant_ids[0][0] ? variant_ids[0][0] : 0;
	}

    var count_variants = function (prod_id) {
        // build order summary string
        var result = "";
        var variant_text = "";
        var value_text = "";
        var value_name = "";
        var mult = 1;
        $("label > input.js_variant_change:checked").each(function () {
            
            //var parent_li = $(this).parents("ul.list-unstyled"); // parent element is needed to get sequence number
            //variant_text = $("li[data-order=" + parent_li.data("order") + "]").children().first().text(); // get corresponding variant text
            value_text = $(this).siblings("span").first().text();
            result += value_text + ", "; // get variant value

        	if ($(this).siblings("span[name='attr_uom_val']").length > 0) {
                mult *= $(this).siblings("span[name='attr_uom_val']").text();
            }
        });
        if ($('#uos_name').text().toLowerCase().indexOf('шт') + 1) {
            $(".variants_result_string").text(result + mult + ' шт.');
        }
        else {
            var uoss = $('.price_per_one_qty').text();
            $(".variants_result_string").text(result + uoss);
        }
        
        // prod_id == null for all events after initialization
        if (!prod_id) {
        	var values = [];
            $('input.js_variant_change:checked, sthisect.js_variant_change').each(function () {
                values.push(+$(this).val());
            });   
            for (var k in variant_ids) {
                if (_.isEmpty(_.difference(variant_ids[k][1], values))) {
                    prod_id = variant_ids[k][0];
                }
            }
        }

        if(!(prod_id || $("input.js_variant_change:checked").first().val()))
          return;

        // XXX value_name always ''
        ajax.jsonRpc("/shop/properties", 'call', {
            'prod_id': prod_id,
            'value_id': $("input.js_variant_change:checked").first().val(),
            'value_name': value_name
        }).then(function (result) {
            $(".prod_property_caption").text(result['prod_property_caption']);
            $(".prod_property").html(result['prod_property']);
            $(".val_property_caption").text(result['val_property_caption']);
            $(".val_property").html(result['val_property']);
            if (result['prod_property_caption'] == ''){
                 $(".prod_property_caption").hide();
            }
            else{
                $(".prod_property_caption").show();
            }
            if (result['prod_property'] == ''){
                 $(".prod_property").hide();
            }
            else{
                $(".prod_property").show();
            }
            if (result['val_property_caption'] == ''){
                 $(".val_property_caption").hide();
            }
            else{
                $(".val_property_caption").show();
            }
            if (result['val_property'] == ''){
                 $(".val_property").hide();
            }
            else{
                $(".val_property").show();
            }

        });
    };
    count_variants(getFirstProd());
    $(".js_add_cart_variants .main-variants-nav > li").click(function () {
        // show corresponding order attributes by clickng on variant
        var order = $(this).attr("data-order");
        $(".js_add_cart_variants .main-variants-nav > li").removeClass("active_variant");
        $(this).addClass("active_variant");
        $(".js_add_cart_variants")
        .find("ul.list-unstyled")
        .css(
            {
                "visibility": "hidden",
                "position": "absolute",
            }
        );
        $("ul[data-order=" + order + "]")
        .css(
            {
                "visibility": "visible",
                "position": "relative",
            }
        );
        count_variants();
    });
    $("label > input.js_variant_change").click(function () {
        count_variants();
    });

});

$(document).scroll(function() { // show top bar on scroll
    var scroll_delta = $(this).scrollTop();
    if (scroll_delta > 45) {
        $('#header-style-1').css("visibility", "visible");
    } else {
        $('#header-style-1').css("visibility", "hidden");
    }
});

/*  moved to website_sale.js $(document).ready(function() {

$('#add_to_cart').on('click', function () {
        var cart = $('.fa-shopping-cart');
        var imgtodrag = $(this).parents().find('img').eq(0);
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
                .css({
                'opacity': '0.8',
                    'position': 'absolute',
                    'height': '375px',
                    'width': '563px',
                    'z-index': '100'
            })
                .appendTo($('body'))
                .animate({
                'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': 150,
                    'height': 150
            }, 1500, 'linear');
            
            imgclone.animate({
                'width': 0,
                    'height': 0
            }, function () {
                $(this).detach()
            });
        }
    });
});
*/

$(document).ready(function(){
   if ($(".my_cart_quantity").html().length > 0) {
     $('.my_cart_quantity').each(function () {$(this).html(Math.round(parseFloat($(this).html())));});
   }
});

});  

function onCaretClick(el) {
	$(el).siblings().first().children('input').css('class','js_variant_select')[0].click();
	$('.a-submit')[0].click();
	//$('.a-submit')[1].click();
}